import arcpy
from arcpy import env
from arcpy.sa import *
import numpy as np
import scipy
from scipy import ndimage
import math
import os
import matplotlib.pyplot as pl

arcpy.CheckOutExtension("Spatial")


def stream_to_line(in_file_path, out_file_path, stream_raster , flow_dir_raster , out_polyline):
    stream_raster_1 = SetNull(Raster('%s'%in_file_path +  '/' + '%s'%stream_raster) == 0 , 1)
    StreamToFeature (stream_raster_1, '%s'%in_file_path +  '/' + '%s'%flow_dir_raster, '%s'%in_file_path +  '/' + '%s'%out_polyline)

def stream_smooth(in_file_path, out_file_path, in_polyline , out_polyline , tolorance):
    in_polyline = '%s'%in_file_path +  '/' + '%s'%in_polyline
    out_polyline = '%s'%out_file_path +  '/' + '%s'%out_polyline
    arcpy.cartography.SmoothLine(in_polyline, out_polyline, "PAEK", tolorance)

def raster_copy(in_file_path, out_file_path, in_raster , out_raster):
    IN_RASTER = Raster('%s'%in_file_path +  '/' + '%s'%in_raster)
    IN_RASTER.save('%s'%out_file_path +  '/' + '%s'%out_raster)

def raster_focal_st(in_file_path, out_file_path, cell_size , org_cell_size , in_raster , out_raster , method , D_ND):
    win_size = round((cell_size / org_cell_size), 0)
    print win_size
    IN_RASTER = Raster('%s'%in_file_path +  '/' + '%s'%in_raster)
    OUT_RASTER = FocalStatistics(IN_RASTER, NbrRectangle(win_size, win_size, "CELL"), method, D_ND)
    OUT_RASTER.save('%s'%out_file_path + '/' + '%s'%out_raster)


    
def raster_pixel_type(in_file_path, out_file_path, in_raster , out_raster):
    OUT_RASTER = '%s'%out_file_path +  '/' + '%s'%out_raster
    IN_RASTER = Raster('%s'%in_file_path +  '/' + '%s'%in_raster)
    arcpy.CopyRaster_management (IN_RASTER , OUT_RASTER, "", "", "", "", "", "32_BIT_FLOAT" , "", "")
      


def raster_resample(in_file_path, out_file_path , cell_size , in_raster , out_raster):
    
    IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster
    OUT_RASTER = '%s'%out_file_path +  '/' + '%s'%out_raster
    
    arcpy.Resample_management (Raster(IN_RASTER), OUT_RASTER , cell_size , "NEAREST")
           
            

def curvature(in_file_path , out_file_path , in_raster , out_raster , unit , option):

    IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster

    IN_RASTER_33 = FocalStatistics(IN_RASTER, NbrRectangle(3, 3, "CELL"), "MEAN", "NODATA")
    
    dem_Array = arcpy.RasterToNumPyArray(IN_RASTER_33)
    corner = arcpy.Point(arcpy.Describe(IN_RASTER_33).Extent.XMin,arcpy.Describe(IN_RASTER_33).Extent.YMin)
    dx = arcpy.Describe(IN_RASTER_33).meanCellWidth

    Ni = dem_Array.shape[0]
    Nj = dem_Array.shape[1]


    if option == 'POLY_GEO'  or  option == 'POLY_LAP':
        L = dx
        
        Z_2 = np.append(dem_Array[0 , :].reshape(1 , Nj) , dem_Array[0 : Ni - 1  , :].reshape(Ni - 1 , Nj) , axis = 0)
        Z_4 = np.append(dem_Array[: , 0].reshape(Ni , 1) , dem_Array[: , 0 : Nj - 1].reshape(Ni , Nj - 1) , axis = 1)
        Z_6 = np.append(dem_Array[: , 1 : Nj].reshape(Ni , Nj - 1) , dem_Array[: , Nj - 1].reshape(Ni , 1) , axis = 1)
        Z_8 = np.append(dem_Array[1 : Ni  , :].reshape(Ni - 1 , Nj) , dem_Array[Ni - 1 , :].reshape(1 , Nj) , axis = 0)
        
        Z_1 = np.append(Z_2[: , 0].reshape(Ni , 1) , Z_2[: , 0 : Nj - 1].reshape(Ni , Nj - 1) , axis = 1)
        Z_3 = np.append(Z_2[: , 1 : Nj].reshape(Ni , Nj - 1) , Z_2[: , Nj - 1].reshape(Ni , 1) , axis = 1)
        Z_5 = np.copy(dem_Array)
        Z_7 = np.append(Z_8[: , 0].reshape(Ni , 1) , Z_8[: , 0 : Nj - 1].reshape(Ni , Nj - 1) , axis = 1)
        Z_9 = np.append(Z_8[: , 1 : Nj].reshape(Ni , Nj - 1) , Z_8[: , Nj - 1].reshape(Ni , 1) , axis = 1)


        #A = ((Z_1  + Z_3  + Z_7  + Z_9 ) / 4  - (Z_2  + Z_4  + Z_6  + Z_8 ) / 2 + Z_5 ) / L ** 4
        #B = ((Z_1  + Z_3  - Z_7  - Z_9 ) /4 - (Z_2  - Z_8 ) /2) / L ** 3
        #C = ((-Z_1  + Z_3  - Z_7  + Z_9 ) /4 + (Z_4  - Z_6 ) / 2) / L ** 3
        D = ((Z_4  + Z_6 )/2 - Z_5 ) / L ** 2
        E = ((Z_2  + Z_8 ) /2 - Z_5 ) / L ** 2
        F = (-Z_1  + Z_3  + Z_7  - Z_9 ) / (4 * L ** 2)
        G = (-Z_4  + Z_6 ) / (2 * L)
        H = (Z_2  - Z_8 ) / (2 * L)
        #I = Z_5

        del Z_1 , Z_2 , Z_3 ,Z_4 , Z_5 , Z_6 , Z_7 , Z_8 , Z_9

        hx = G
        hxx = 2 * D
        hxy = F
        hy = H
        hyy = 2 * E

        del G , D , F ,H , E
        
        if option == 'POLY_GEO':
            curve_Array = (hxx * hy ** 2 - 2 * hxy * hx * hy + hyy * hx ** 2) / ((hx ** 2 + hy ** 2) * (1 + hx ** 2 + hy ** 2) ** 0.5)
        else:
            curve_Array = hxx + hyy

        del hx , hxx , hxy ,hy , hyy
        
        if unit == 'ft':
            curve_Array = curve_Array * (1 / 0.3048)
    else:
        grad_x , grad_y =  np.gradient(dem_Array , dx)
        slope_Array = np.sqrt(grad_x ** 2 + grad_y ** 2)
        arcpy.NumPyArrayToRaster(slope_Array , corner , dx , dx).save('%s'%out_file_path +  '/slope.tif') 
        if option == 'GEO':
            grad_x = grad_x /slope_Array
            grad_y = grad_y /slope_Array
            
            grad_x[slope_Array == 0.0] = 0.0
            grad_y[slope_Array == 0.0] = 0.0
        grad_xx , _ = np.gradient(grad_x , dx)
        _, grad_yy = np.gradient(grad_y , dx)
        curve_Array = grad_xx + grad_yy

    curve_Array[np.abs(curve_Array) >= 2] = np.nan
    arcpy.NumPyArrayToRaster(curve_Array , corner , dx , dx).save('%s'%out_file_path + '/' + '%s'%out_raster)

            

def fill_dem(in_file_path, out_file_path , in_raster , out_raster):
    
    IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster
    
    Fill(IN_RASTER).save('%s'%out_file_path +  '/' + '%s'%out_raster)
               

def cut_dem(in_file_path, out_file_path , in_raster , bond , out_raster):
        
    IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster
    BOND =  '%s'%in_file_path +  '/' + '%s'%bond
    
    Con(Raster(BOND)  == 1 , Raster(IN_RASTER)).save('%s'%out_file_path +  '/' + '%s'%out_raster)
            


def flow_dir(in_file_path, out_file_path , in_raster ,out_raster):
   
    IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster
    
    FlowDirection(IN_RASTER, "FORCE").save('%s'%out_file_path +  '/' + '%s'%out_raster)
                 

def flow_acc(in_file_path, out_file_path , dir_raster , out_raster):
   

    dataType = "INTEGER"
    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%dir_raster

    FlowAccumulation(DIR_RASTER, "" ,dataType).save('%s'%out_file_path +  '/' + '%s'%out_raster)
            
def flow_acc_bond(in_file_path, out_file_path , acc_tresh , acc_raster , out_raster , sub , step , deb_mode):
    
    ACC_RASTER = '%s'%in_file_path +  '/' + '%s'%acc_raster
    Con((Raster(ACC_RASTER)  >= acc_tresh) , 1 , 0).save('%s'%out_file_path + '/' + '%s'%out_raster)

            
def flow_acc_max(in_file_path, out_file_path , in_raster, sub , step , deb_mode):
    
    max_acc = arcpy.GetRasterProperties_management('%s'%in_file_path +  '/' + '%s'%in_raster , "MAXIMUM")
    return int(max_acc.getOutput(0))
    


def stream_order(in_file_path, out_file_path , st_raster , dir_raster , out_raster):
    
    ST_RASTER = '%s'%in_file_path +  '/' + '%s'%st_raster
    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%dir_raster
    
    StreamOrder(ST_RASTER , DIR_RASTER).save('%s'%out_file_path +   '/' + '%s'%out_raster)


def CC_dis(in_file_path , curve_raster , ave_k , figure_name):
    CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%curve_raster
    
    raster = Abs(Raster(CURVE_RASTER))
    curve_Array = arcpy.RasterToNumPyArray(raster)
    corner = arcpy.Point(arcpy.Describe(raster).Extent.XMin,arcpy.Describe(raster).Extent.YMin)
    dx = arcpy.Describe(raster).meanCellWidth

    max_curve = arcpy.GetRasterProperties_management(raster, "MAXIMUM")
    max_curve = float(max_curve.getOutput(0))

    min_curve = arcpy.GetRasterProperties_management(raster, "MINIMUM")
    min_curve = float(min_curve.getOutput(0))

    num_bins = int((max_curve - min_curve) / 0.0005) + 1

    #print max_curve , min_curve , num_bins
    
    cumfreqs, lowlim, binsize, extrapoints = scipy.stats.cumfreq(curve_Array, numbins = num_bins , defaultreallimits=(min_curve , max_curve))

    

    max_cell = np.max(cumfreqs)

    curve_prob = np.zeros((1 , 3))
    for i in range (0 , num_bins):
        k = np.abs(lowlim + binsize * i)
        if k <= 3 * ave_k:
            P = 1 - cumfreqs[i] / max_cell
            curve_prob = np.copy(np.append(curve_prob , np.array([k , P , 0]).reshape(1 , 3) , axis = 0))
    curve_prob = np.delete(curve_prob, 0, 0)
            
    num_point = curve_prob.shape[0]
    for i in range (0 , num_point - 1):
        curve_prob[i , 2] = curve_prob[i , 1] - curve_prob[i + 1 , 1]
    curve_prob = np.delete(curve_prob, num_point - 1, 0)

    threshold = general_linear_fit_log(curve_prob[: , 0] , curve_prob[: , 2] , in_file_path ,figure_name)

    return threshold

def linear_fit(x , y):
    x_bar = np.average(x)
    y_bar = np.average(y)
    xy_bar = np.average(x * y)
    x2_bar = np.average(x ** 2)
    y2_bar = np.average(y ** 2)

    a = (xy_bar  - x_bar * y_bar) / (x2_bar - x_bar ** 2)
    b = y_bar - a * x_bar
    R2 = (xy_bar  - x_bar * y_bar) / ((x2_bar - x_bar ** 2) * (y2_bar - y_bar ** 2)) ** 0.5

    return a , b , R2

def general_linear_fit_log(x , y , out_file_path , figure_name):
    num_point = x.shape[0]
    min_E = 10 ** 10
    for t_i in range (1 , num_point - 1):
        x_1 = x[0 : t_i - 1]
        y_1 = y[0 : t_i - 1]

        x_2 = x[t_i : num_point - 1]
        y_2 = y[t_i : num_point - 1]
        
        a_1 , b_1 , R2_1 = linear_fit(x_1, y_1)
        y_1_s = a_1 * x_1 + b_1
        E_1 = np.sum(np.abs(y_1 - y_1_s))

        a_2 , b_2 , R2_2 = linear_fit(np.log10(x_2), np.log10(y_2))
        y_2_s = 10 ** (a_2 * np.log10(x_2) + b_2)
        E_2 = np.sum(np.abs(y_2 - y_2_s))
        
        if  E_1 + E_2 <= min_E:
            best_t_i = t_i
            best_a_1 = a_1
            best_a_2 = a_2
            best_b_1 = b_1
            best_b_2 = b_2
            min_E = E_1 + E_2

    t_i = best_t_i
    k_thresh = (x[t_i] + x[t_i - 1]) / 2
    fig = pl.figure()
    x_1 = x[0 : t_i - 1]
    y_1 = y[0 : t_i - 1]
    pl.plot(x_1 , y_1 , 'ro')
    pl.plot(x_1 , best_a_1 * x_1 + best_b_1 , 'r-')

    x_2 = x[t_i : num_point - 1]
    y_2 = y[t_i : num_point - 1]
    pl.plot(x_2 , y_2 , 'go')
    pl.plot(x_2 , 10 ** (best_a_2 * np.log10(x_2) + best_b_2) , 'g-')
    pl.plot(np.ones((10 , 1)) * k_thresh , np.arange(0 , 0.1 , 0.01) , 'k--' , linewidth = 2)
    pl.grid(b=True, which='minor', color='k', linestyle='--')
    pl.grid(b=True, which='major', color='k', linestyle='-')
    pl.yticks(fontsize = 16)
    pl.xticks(fontsize = 16)
    pl.xlabel('Curvature (1/m)', fontsize=20)
    pl.ylabel('Probability', fontsize=20)
    fig.savefig('%s'%out_file_path  +  '/' + '%s'%figure_name)
    
    axes = pl.gca()

    return k_thresh
    
    

def general_linear_fit_lin(x , y , out_file_path , figure_name):
    num_point = x.shape[0]
    min_E = 10 ** 10
    for t_i in range (1 , num_point - 1):
        x_1 = x[0 : t_i - 1]
        y_1 = y[0 : t_i - 1]

        x_2 = x[t_i : num_point - 1]
        y_2 = y[t_i : num_point - 1]
        
        a_1 , b_1 , R2_1 = linear_fit(x_1, y_1)
        y_1_s = a_1 * x_1 + b_1
        E_1 = np.sum(np.abs(y_1 - y_1_s))

        a_2 , b_2 , R2_2 = linear_fit(x_2, y_2)
        y_2_s = a_2 * x_2 + b_2
        E_2 = np.sum(np.abs(y_2 - y_2_s))
        
        if  E_1 + E_2 <= min_E:
            best_t_i = t_i
            best_a_1 = a_1
            best_a_2 = a_2
            best_b_1 = b_1
            best_b_2 = b_2
            min_E = E_1 + E_2

    t_i = best_t_i
    k_thresh = (x[t_i] + x[t_i - 1]) / 2
    fig = pl.figure()
    x_1 = x[0 : t_i - 1]
    y_1 = y[0 : t_i - 1]
    pl.plot(x_1 , y_1 , 'ro')
    pl.plot(x_1 , best_a_1 * x_1 + best_b_1 , 'r-')

    x_2 = x[t_i : num_point - 1]
    y_2 = y[t_i : num_point - 1]
    pl.plot(x_2 , y_2 , 'go')
    pl.plot(x_2 , best_a_2 * x_2 + best_b_2 , 'g-')
    pl.plot(np.ones((10 , 1)) * k_thresh , np.arange(0 , 1 , 0.1) , 'k--' , linewidth = 2)
    pl.grid(b=True, which='minor', color='k', linestyle='--')
    pl.grid(b=True, which='major', color='k', linestyle='-')
    pl.yticks(fontsize = 16)
    pl.xticks(fontsize = 16)
    pl.xlabel('k * Log(A) ', fontsize=20)
    pl.ylabel('Probability', fontsize=20)
    fig.savefig('%s'%out_file_path  +  '/' + '%s'%figure_name)
    
    axes = pl.gca()

    return k_thresh

            
def valley_bond(in_file_path, out_file_path , valley_raster , stream_raster , buffer_size):

    VALLEY_RASTER = '%s'%in_file_path +  '/' + '%s'%valley_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster

    thick_stream_raster = FocalStatistics(Raster(STREAM_RASTER),  NbrCircle(buffer_size ,"CELL"), 'MAXIMUM', "DATA")
    thick_valley_raster = FocalStatistics(Raster(VALLEY_RASTER),  NbrCircle(buffer_size ,"CELL"), 'MAXIMUM', "DATA")

    bond_1 = Con(IsNull(thick_stream_raster) , 0 , thick_stream_raster)
    bond_2 = Con(IsNull(thick_valley_raster) , 0 , thick_valley_raster)

    #final_valley_bond = bond_1 + bond_2

    final_valley_bond = Con(((bond_1 == 1) | (bond_2 == 1)), 1)

    final_valley_bond.save('%s'%out_file_path +  '/FVB.tif')
    thick_stream_raster.save('%s'%out_file_path +  '/TS.tif')
    thick_valley_raster.save('%s'%out_file_path +  '/TVB.tif')

    
    


################################################################################
################################################################################
    
def move_cal(m , i , j):
    if m == 1:
        new_i = i
        new_j = j + 1
    elif m == 2:
        new_i = i + 1
        new_j = j + 1
    elif m == 4:
        new_i = i + 1
        new_j = j 
    elif m == 8:
        new_i = i + 1
        new_j = j - 1
    elif m == 16:
        new_i = i
        new_j = j - 1
    elif m == 32:
        new_i = i - 1
        new_j = j - 1
    elif m == 64:
        new_i = i - 1
        new_j = j
    elif m == 128:
        new_i = i - 1
        new_j = j + 1
    else:
        new_i = i
        new_j = j
    return new_i, new_j

#############################################################################
#############################################################################

def work_check(sub):
    ORG_DEM = 'C:/miladhooshyar/Spring2014/GIS/archydro_curve/Valley_Network/CDEM' + '/'+ 'subDEM_' +  str(sub) + '.tif'
    TT = True
    while TT == True:
        try:
            org_ext = arcpy.Describe(ORG_DEM).Extent.XMin
            env_path = env.workspace
            ext = env.extent
            sk_path = env.scratchWorkspace
            TT = False
        except:
            print 'Fail_Workspace'
            TT = True
    return os.path.basename(env_path) == str(sub) , org_ext == ext.XMin , os.path.basename(sk_path) == 'Temp' + str(sub)

#############################################################################
#############################################################################

def mod_area_curve(area_curve):
    num_point = area_curve.shape[0]
    mod_area_curve = np.zeros((1 , 2))
    
    cur_size = area_curve[0 , 0]
    i = 0
    ave_curve = 0
    counter = 0
    while i < num_point - 1:
        #print area_curve[i , 0] , cur_size
        if area_curve[i , 0] == cur_size:
            ave_curve = ave_curve + area_curve[i , 1]
            counter = counter + 1
            i = i + 1
        else:
            ave_curve = ave_curve / counter
            mod_area_curve = np.copy(np.append(mod_area_curve , np.array([cur_size , ave_curve]).reshape(1 , 2) , axis = 0))
            #print cur_size , ave_curve
            ave_curve = 0
            counter = 0
            cur_size = area_curve[i , 0]
            
        
        
    mod_area_curve = np.delete(mod_area_curve, 0 , 0 )

    return mod_area_curve
            
#############################################################################
#############################################################################

def segment_size_curvature(in_file_path , curve_raster):
    CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%curve_raster
    curve_Array = arcpy.RasterToNumPyArray(CURVE_RASTER  , nodata_to_value=0)

    min_c = min(abs(np.nanmin(curve_Array)) , np.nanmax(curve_Array))
    
    max_num_label = 0
    for i in range (1 , 200):
        k_tresh_valley = 1 * ( float(i) * 0.0005 - 0.02)
        k_tresh_ridge = -1 * k_tresh_valley

        if abs(k_tresh_valley) <= min_c:
            Val_Array = np.where(curve_Array >= k_tresh_valley, 1, 0)
            Rid_Array = np.where(curve_Array <= k_tresh_ridge, 1, 0)
            
            Lab_Val_Array, num_label_val = ndimage.label(Val_Array , structure = np.ones((3 , 3)))
            Lab_Rid_Array, num_label_rid = ndimage.label(Rid_Array , structure = np.ones((3 , 3)))
            
            labels_val = np.arange(1, num_label_val + 1)
            labels_rid = np.arange(1, num_label_rid + 1)

            area_label_val = ndimage.labeled_comprehension(Val_Array, Lab_Val_Array, labels_val, np.sum, int, 0)
            area_label_rid = ndimage.labeled_comprehension(Rid_Array, Lab_Rid_Array, labels_rid, np.sum, int, 0)
            
            
            num_sig_label = np.sum(area_label_val >= 1) + np.sum(area_label_rid >= 1)

            if num_sig_label >= max_num_label:
                max_num_label = num_sig_label
                valley_thresh = k_tresh_valley
            

    return  valley_thresh
    
