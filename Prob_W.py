import arcpy
from arcpy import env
from arcpy.sa import *
import numpy as np
import scipy
from scipy import ndimage
from scipy.cluster.vq import vq, kmeans, whiten
from scipy.stats import norm
import math
import os
import matplotlib.pyplot as pl
import base_network_pro
import base_network_fun
import sklearn
from sklearn import mixture
from scipy import sparse
import cv2

arcpy.CheckOutExtension("Spatial")

def edge_find(in_file_path , int_raster , val_raster , lower , upper , lower_int_thresh , upper_int_thresh):

    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    VAL_RASTER = '%s'%in_file_path +  '/' + '%s'%val_raster
    base_network_fun.raster_focal_st(in_file_path, in_file_path, 3 , 1 , 'INT_NV.tif' , 'INT_NV_F.tif' , "MINIMUM" , "NODATA")
    INT_F_RASTER = '%s'%in_file_path +  '/INT_NV_F.tif'

    corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
    dx = arcpy.Describe(INT_RASTER).meanCellWidth

    val_Array = arcpy.RasterToNumPyArray(VAL_RASTER  , nodata_to_value= 0)
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER , nodata_to_value= 0)
    int_f_Array = arcpy.RasterToNumPyArray(INT_F_RASTER , nodata_to_value= 0)
    img = np.uint8(int_Array)
    #img = cv2.imread('messi5.jpg',0)

##    img_1 = img[img > 0]
##    v = np.median(img_1)
##    lower = int(max(0, (1.0 - sigma) * v))
##    upper = int(min(255, (1.0 + sigma) * v))
##    print lower , upper
    
    edges = cv2.Canny(img , lower , upper)
    edges = edges + 1
    edges = np.where((edges == 0) & (int_f_Array > 0), 1 , 0)
    arcpy.NumPyArrayToRaster(edges , corner,dx,dx).save('%s'%in_file_path +  '/edge.tif')
    refine_edge(in_file_path, in_file_path , 'edge.tif' , 'DEM_g.tif')

    REF_EDGE_RASTER = '%s'%in_file_path +  '/refine_edge.tif'
    edges = arcpy.RasterToNumPyArray(REF_EDGE_RASTER  , nodata_to_value= 0)

    #edges = np.where(int_Array <= lower_int_thresh, 1 , 0)

    edges = np.where((val_Array == 1) & (int_f_Array > 0) & (((edges == 1) & (int_f_Array <= upper_int_thresh)) | ((int_f_Array <= lower_int_thresh))), 1 , 0)

    arcpy.NumPyArrayToRaster(edges , corner,dx,dx).save('%s'%in_file_path +  '/WMD.tif')

def intensity_norm(in_file_path , int_raster , cell_size , org_cell_size):
    
##    base_network_fun.raster_focal_st(in_file_path, in_file_path, cell_size , org_cell_size , int_raster , 'AVE_INT.tif' , "MEAN")
##    base_network_fun.raster_focal_st(in_file_path, in_file_path, cell_size , org_cell_size , int_raster , 'STD_INT.tif' , "STD")
    
    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
    dx = arcpy.Describe(INT_RASTER).meanCellWidth

    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value= -1)

    number_row =  int_Array.shape[0]
    number_col = int_Array.shape[1]

    win_size = int(cell_size / (2 * org_cell_size))

    print win_size

    ave_Array = np.zeros_like(int_Array)
    std_Array = np.zeros_like(int_Array)
    norm_fit_Array = np.zeros_like(int_Array)
    

    for index , value in np.ndenumerate(int_Array):
        if value != -1:
            upper_i = min(index[0] + win_size , number_row)
            lower_i = max(index[0] - win_size , 0)
            upper_j = min(index[1] + win_size , number_col)
            lower_j = max(index[1] - win_size , 0)

            int_data = int_Array[lower_i : upper_i , lower_j : upper_j]
            num_data = int_data.shape[0] * int_data.shape[1]
            int_data =  int_data.reshape(num_data , 1)
            int_data = int_data[int_data > 0]

            ave_Array[index] = np.mean(int_data)
            std_Array[index] = np.std(int_data)

            bins = range(int(np.min(int_data) - 2.5) , int(np.max(int_data) + 2.5) , 5)
            act_prob , bins = np.histogram(int_data , bins)
            bins = np.asarray(bins)
            bins_p = bins[1 : bins.shape[0]]
            bins = np.delete(bins, bins.shape[0] - 1, 0)
            bins = (bins + bins_p) / 2
            
            act_prob = np.asarray(act_prob)
            act_prob = act_prob.astype(np.float64)
            act_prob = act_prob / np.sum(act_prob)

            est_prob = norm.cdf(bins + 2.5 , loc = ave_Array[index] , scale = std_Array[index]) - norm.cdf(bins - 2.5 , loc = ave_Array[index] , scale = std_Array[index])


            norm_fit_Array[index] = np.sum(np.abs(act_prob - est_prob)) / act_prob.shape


##            if int_data.shape[0] > 80 and np.min(int_data) > 70:
##                for i in int_data:
##                    print i
##                sss



    arcpy.NumPyArrayToRaster(ave_Array , corner,dx,dx , value_to_nodata=0).save('%s'%in_file_path +  '/AVE_INT.tif')
    arcpy.NumPyArrayToRaster(std_Array , corner,dx,dx , value_to_nodata=0).save('%s'%in_file_path +  '/STD_INT.tif')
    arcpy.NumPyArrayToRaster(norm_fit_Array , corner,dx,dx , value_to_nodata=0).save('%s'%in_file_path +  '/NORM_FIT.tif') 
        
    
##    INT_F_RASTER = '%s'%in_file_path +  '/Fo_INT_C.tif'
##
##    INT_NORM_RASTER = Raster(INT_RASTER) / Raster(INT_F_RASTER)
##
##    INT_NORM_RASTER.save('%s'%in_file_path +  '/N_INT_C.tif')
    

def veg_bond(in_file_path , all_raster , ground_raster , diff_thresh):
    ALL_RASTER = '%s'%in_file_path +  '/' + '%s'%all_raster
    GROUND_RASTER = '%s'%in_file_path +  '/' + '%s'%ground_raster

    DIF_RASTER = Raster(ALL_RASTER) - Raster(GROUND_RASTER)
    NO_VEG = Con(DIF_RASTER < diff_thresh , 1 , 0)

    NO_VEG.save('%s'%in_file_path +  '/No_veg.tif')

    

    
    
##    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value=0)
##    int_Array = int_Array.reshape(int_Array.shape[0] * int_Array.shape[1])
##    int_Array = int_Array[int_Array > 0]
##    int_Array =  int_Array.reshape(int_Array.shape[0] , 1)



def prob_fit(in_file_path , int_raster):
    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value=0)
    int_Array = int_Array.reshape(int_Array.shape[0] * int_Array.shape[1])
    int_Array = int_Array[int_Array > 0]
    int_Array =  int_Array.reshape(int_Array.shape[0] , 1)
    
##    gmix = mixture.GMM(n_components = 3, covariance_type='full' , n_iter = 10000 , tol = 0.00000001)
##    gmix.fit(int_Array)
##
##    fitting_result = np.zeros((3 , 3))

##    print gmix.weights_
##    print gmix.means_
##    print gmix.covars_


##    for i in range(0 , 3):
##        fitting_result[0 , i] = gmix.weights_[np.argmax(gmix.means_)]
##        fitting_result[1 , i] = gmix.means_[np.argmax(gmix.means_)]
##        fitting_result[2 , i] = gmix.covars_[np.argmax(gmix.means_)] ** 0.5
##
##        gmix.means_[np.argmax(gmix.means_)] = 0
##    
##    print fitting_result


    bins = range(0 , np.max(int_Array) , 1)
    hist , xx = np.histogram(int_Array , bins)
    pl.plot(xx[0:xx.size - 1] , hist)
    for i in range (0 , len(bins)):
        print bins[i] , float(hist[i]) / np.sum(hist)

    return fitting_result

def wet_bond_simple(in_file_path, out_file_path , int_raster , thresh):


    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
    dx = arcpy.Describe(INT_RASTER).meanCellWidth
    
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value=0)

    WMD_Array = WMD_Array = np.where((int_Array <= thresh) & (int_Array > 0) > 0 , 3, 0)

    arcpy.NumPyArrayToRaster(WMD_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/WMD.tif')

def wet_bond(in_file_path, out_file_path , int_raster , fitting_result):

    W_d = fitting_result[0 , 0]
    Ave_d = fitting_result[1 , 0]
    Sigma_d = fitting_result[2 , 0]

    W_m = fitting_result[0 , 1]
    Ave_m = fitting_result[1 , 1]
    Sigma_m = fitting_result[2 , 1]

    W_w = fitting_result[0 , 2]
    Ave_w = fitting_result[1 , 2]
    Sigma_w = fitting_result[2 , 2]

    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
    dx = arcpy.Describe(INT_RASTER).meanCellWidth
    
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value=0)

    
    Prod_w = (norm.cdf(int_Array , Ave_w , Sigma_w) - norm.cdf(int_Array - 2 , Ave_w , Sigma_w)) * W_w
    Prod_w = np.where(int_Array == 0, 0 , Prod_w)
    Prod_m = (norm.cdf(int_Array , Ave_m , Sigma_m) - norm.cdf(int_Array - 2 , Ave_m , Sigma_m)) * W_m
    Prod_m = np.where(int_Array == 0, 0 , Prod_m)
    Prod_d = (norm.cdf(int_Array , Ave_d , Sigma_d) - norm.cdf(int_Array - 2 , Ave_d , Sigma_d)) * W_d
    Prod_d = np.where(int_Array == 0, 0 , Prod_d)

    del int_Array

    
    N_Prod_w = Prod_w / (Prod_w + Prod_m + Prod_d)
    arcpy.NumPyArrayToRaster(N_Prod_w , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/Pw.tif')
    N_Prod_m = Prod_m / (Prod_w + Prod_m + Prod_d)
    arcpy.NumPyArrayToRaster(N_Prod_m , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/Pm.tif')
    N_Prod_d = Prod_d / (Prod_w + Prod_m + Prod_d)
    arcpy.NumPyArrayToRaster(N_Prod_d , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/Pd.tif')

    del Prod_w , Prod_m , Prod_d
    #WMD_Array = np.where((N_Prod_w >= 0.8) , 3, 0)
    WMD_Array = np.where((N_Prod_w >= N_Prod_m) & (N_Prod_w >= N_Prod_d), 3, 0)
    WMD_Array = np.where((N_Prod_m >= N_Prod_w) & (N_Prod_m >= N_Prod_d), 2, WMD_Array)
    WMD_Array = np.where((N_Prod_d >= N_Prod_w) & (N_Prod_d >= N_Prod_m), 1, WMD_Array)

    del N_Prod_w , N_Prod_m , N_Prod_d

    arcpy.NumPyArrayToRaster(WMD_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/WMD.tif')


def refine_edge(in_file_path, out_file_path , edge_raster , ele_raster):
    
    EDGE_RASTER = '%s'%in_file_path +  '/' + '%s'%edge_raster
    ELE_RASTER = '%s'%in_file_path +  '/' + '%s'%ele_raster
   
    edge_Array = arcpy.RasterToNumPyArray(EDGE_RASTER  , nodata_to_value=0)
    ele_raster = arcpy.RasterToNumPyArray(ELE_RASTER  , nodata_to_value=0)

    corner = arcpy.Point(arcpy.Describe(EDGE_RASTER).Extent.XMin,arcpy.Describe(EDGE_RASTER).Extent.YMin)
    dx = arcpy.Describe(EDGE_RASTER).meanCellWidth

    edge_Array = np.where((edge_Array == 1) , 1, 0)

    Lab_e_Array, num_label = ndimage.label(edge_Array , structure = np.ones((3 , 3)))
    labels = np.arange(1, num_label + 1)
    area_label = ndimage.labeled_comprehension(edge_Array, Lab_e_Array, labels, np.sum, float, 0)
    max_ele_label = ndimage.labeled_comprehension(ele_raster, Lab_e_Array, labels, np.max , float, 0)
    min_ele_label = ndimage.labeled_comprehension(ele_raster, Lab_e_Array, labels, np.min , float, 0)

    area = np.zeros_like(ele_raster)
    min_ele = np.zeros_like(ele_raster)
    max_ele = np.zeros_like(ele_raster)
    ele_per_area = np.zeros_like(ele_raster)
    for index , value in np.ndenumerate(Lab_e_Array):
        if value > 1:
            area[index] = area_label[int(value) - 1]
            max_ele[index] = max_ele_label[int(value) - 1]
            min_ele[index] = min_ele_label[int(value) - 1]
            ele_per_area[index] = (max_ele[index] - min_ele[index]) / area[index]
    
    edge_Array = np.where((area > 5) & (edge_Array > 0) , 1, 0)
    edge_Array = np.where( ((max_ele - min_ele) >= 1) & (edge_Array > 0) , 1, 0)
    
    arcpy.NumPyArrayToRaster(edge_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/refine_edge.tif')
    
    
def refine_wet(in_file_path, out_file_path , wet_raster):
    
    WET_RASTER = '%s'%in_file_path +  '/' + '%s'%wet_raster
   
   
    wet_Array = arcpy.RasterToNumPyArray(WET_RASTER  , nodata_to_value=0)

    corner = arcpy.Point(arcpy.Describe(WET_RASTER).Extent.XMin,arcpy.Describe(WET_RASTER).Extent.YMin)
    dx = arcpy.Describe(WET_RASTER).meanCellWidth

    w_Array_0 = np.where((wet_Array == 1) , 1, 0)

    Lab_w_Array, num_label = ndimage.label(w_Array_0 , structure = np.ones((3 , 3)))
    labels = np.arange(1, num_label + 1)
    area_label = ndimage.labeled_comprehension(w_Array_0, Lab_w_Array, labels, np.sum, int, 0)

    area = np.zeros_like(wet_Array)

    for index , value in np.ndenumerate(Lab_w_Array):
        if value > 1:
            area[index] = area_label[int(value) - 1]
    
    w_Array_0 = np.where((area > 5) , 1, 0)
    
    arcpy.NumPyArrayToRaster(w_Array_0 , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/wet_bond.tif')



    

##    # delete small segments
##    Lab_w_Array, num_label = ndimage.label(w_Array_0 , structure = np.ones((3 , 3)))
##    labels = np.arange(1, num_label + 1)
##    area_label = ndimage.labeled_comprehension(w_Array_0, Lab_w_Array, labels, np.sum, float, 0)
##    for index , value in np.ndenumerate(Lab_w_Array):
##            if value > 1:
##                if area_label[int(value) - 1] <= 5:
##                    w_Array_0[index] = 0
##    
####    #Closing
####    w_Array_0 = ndimage.binary_closing(w_Array_0 , structure=np.ones((3,3))).astype(w_Array_0.dtype)
####    #Filling
####    w_Array_0 = ndimage.binary_fill_holes(w_Array_0 , structure=np.ones((3,3))).astype(w_Array_0.dtype)
##    
##    er_w_Array = ndimage.binary_erosion(w_Array_0).astype(w_Array_0.dtype)
##    bound_w_Array = np.where((w_Array_0 == 1) & (er_w_Array == 0), 1, 0)
##    
##    Lab_w_Array, num_label = ndimage.label(w_Array_0 , structure = np.ones((3 , 3)))
##    labels = np.arange(1, num_label + 1)
##    area_label = ndimage.labeled_comprehension(w_Array_0, Lab_w_Array, labels, np.sum, float, 0)
##    per_label = ndimage.labeled_comprehension(bound_w_Array, Lab_w_Array, labels, np.sum, float, 0)
##    in_valley_label = ndimage.labeled_comprehension(valley_Array, Lab_w_Array, labels, np.sum, float, 0)
##    
##    len_label = np.zeros_like(area_label)
##    wid_label = np.zeros_like(area_label)
##    for i in range (0 , len(area_label)):
##        temp_root = np.roots([1 , -1 * per_label[i] / 2 , area_label[i]])
##        len_label[i] = np.max(temp_root)
##        wid_label[i] = np.min(temp_root)
##
##    num_label = len(len_label)
##
##    area = np.zeros_like(wet_Array)
##    per = np.zeros_like(wet_Array)
##    width = np.zeros_like(wet_Array)
##    length = np.zeros_like(wet_Array)
##    in_valley = np.zeros_like(wet_Array)
##    
##    for index , value in np.ndenumerate(Lab_w_Array):
##        if value > 1:
##            area[index] = area_label[int(value) - 1]
##            per[index] = per_label[int(value) - 1]
##            length[index] = len_label[int(value) - 1]
##            width[index] = wid_label[int(value) - 1]
##            in_valley[index] = in_valley_label[int(value) - 1]
##
##    arcpy.NumPyArrayToRaster(in_valley , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/in_valley.tif')
##    arcpy.NumPyArrayToRaster(area , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/area.tif')
##    arcpy.NumPyArrayToRaster(per , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/per.tif')
##    arcpy.NumPyArrayToRaster(width , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/width.tif')
##    arcpy.NumPyArrayToRaster(length , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/length.tif')

def make_stream_simple(in_file_path , wet_raster , stream_raster , flow_dir_raster , org_cell_size , tolerance):
    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
    WET_RASTER = '%s'%in_file_path +  '/' + '%s'%wet_raster

    base_network_fun.raster_focal_st(in_file_path, in_file_path, tolerance , org_cell_size , wet_raster , 'Foc_wet_bond.tif' , "MAXIMUM" ,"DATA")

    FOC_WET_RASTER = '%s'%in_file_path +  '/Foc_wet_bond.tif'

    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    foc_wet_Array = arcpy.RasterToNumPyArray(FOC_WET_RASTER , nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth

    num_row = stream_Array.shape[0]
    num_col = stream_Array.shape[1]

    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    heads_list = base_network_pro.find_head(Sp_stream_Array , dir_Array)

    wet_stream = np.zeros_like(stream_Array)

    p = -1
    for index in heads_list:
        p = p + 1
        print p
        list_passed , last_point , length = base_network_pro.move_forward_connect_2(index , stream_Array , dir_Array , num_row , num_col)
        i  = 0
        for point in list_passed:
            if foc_wet_Array[point] == 1:
                wet_stream[point] =  1

    wet_raster = arcpy.NumPyArrayToRaster(wet_stream,corner, dx ,dx)
    wet_raster.save('%s'%in_file_path +  '/WS.tif')
        

##def make_stream(in_file_path , wet_raster , stream_raster , acc_raster , tolerance):
##        WET_RASTER = '%s'%in_file_path +  '/' + '%s'%wet_raster
##
##        STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
##        ACC_RASTER = '%s'%in_file_path +  '/' + '%s'%acc_raster
##
##        stream_raster = SnapPourPoint(WET_RASTER, ACC_RASTER, tolerance)
##
##        stream_raster_c = Con((stream_raster == 1) & (Raster(STREAM_RASTER) == 1) , 1)
##        stream_raster_c.save('%s'%in_file_path +  '/WS.tif')



        

def segment_clustering(in_file_path, out_file_path , area_raster , in_valley_raster , length_raster):

    AREA_RASTER = '%s'%in_file_path +  '/' + '%s'%area_raster
    IN_VALLEY_RASTER = '%s'%in_file_path +  '/' + '%s'%in_valley_raster
    LENGTH_RASTER = '%s'%in_file_path +  '/' + '%s'%length_raster

    corner = arcpy.Point(arcpy.Describe(AREA_RASTER).Extent.XMin,arcpy.Describe(AREA_RASTER).Extent.YMin)
    dx = arcpy.Describe(AREA_RASTER).meanCellWidth
    
    area_Array = arcpy.RasterToNumPyArray(AREA_RASTER  , nodata_to_value=0)
    in_valley_Array = arcpy.RasterToNumPyArray(IN_VALLEY_RASTER  , nodata_to_value=0)
    length_Array = arcpy.RasterToNumPyArray(LENGTH_RASTER  , nodata_to_value=0)

    ration_Array = in_valley_Array / area_Array

    lab_Array_1 = np.where((ration_Array <= 0.1), - 2 , 0)
    lab_Array_2 = np.where((in_valley_Array >= 1000), 1 , 0)
    lab_Array_3 = np.where((ration_Array >= 0.7), 1 , 0)
    
    lab_Array = lab_Array_1 + lab_Array_2 + lab_Array_3

    arcpy.NumPyArrayToRaster(lab_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/cluster_1.tif') 
    

    
##    bin_Array = np.where((area_Array > 0), 1, 0)
##    lab_Array, num_label = ndimage.label(bin_Array , structure = np.ones((3 , 3)))
##    labels = np.arange(1, num_label + 1)
##
##    area_label = ndimage.labeled_comprehension(lab_Array, area_Array, labels, np.mean, float, 0)
##    in_valley_label = ndimage.labeled_comprehension(lab_Array, in_valley_Array, labels, np.mean, float, 0)
##    length_label = ndimage.labeled_comprehension(lab_Array, length_Array, labels, np.mean, float, 0)
##
##    num_label = len(length_label)
##    
##    obs = np.zeros((num_label , 2))
##    obs[: , 0] = area_label
##    obs[: , 1] = in_valley_label
##    #obs[: , 2] = length_label
##    
##    w_obs = whiten(obs)
##
##    cent , _ = kmeans(w_obs , 3)
##    print cent
##    clu_label , _ = vq(w_obs , cent)
##    
##    clu = np.zeros_like(area_Array)
##    for index , value in np.ndenumerate(lab_Array):
##        if value > 1:
##            clu[index] = clu_label[int(value) - 1] + 1
##
##    
##    arcpy.NumPyArrayToRaster(clu , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/cluster_1.tif')        

        
    
    
    
