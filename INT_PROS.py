import arcpy
from arcpy import env
from arcpy.sa import *
import numpy as np
import scipy
from scipy import ndimage
from scipy.cluster.vq import vq, kmeans, whiten
import math
import os
import matplotlib.pyplot as pl

arcpy.CheckOutExtension("Spatial")

    
def wet_bond(in_file_path, out_file_path , int_raster , ele_raster , int_thresh):
    
    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    ELE_RASTER = '%s'%in_file_path +  '/' + '%s'%ele_raster
    
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value=0)
    ele_Array = arcpy.RasterToNumPyArray(ELE_RASTER  , nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
    dx = arcpy.Describe(INT_RASTER).meanCellWidth

    w_Array_0 = np.where((int_Array <= int_thresh) & (int_Array > 0), 1, 0)

    # delete small segments
    Lab_w_Array, num_label = ndimage.label(w_Array_0 , structure = np.ones((3 , 3)))
    labels = np.arange(1, num_label + 1)
    area_label = ndimage.labeled_comprehension(w_Array_0, Lab_w_Array, labels, np.sum, float, 0)
    for index , value in np.ndenumerate(Lab_w_Array):
            if value > 1:
                if area_label[int(value) - 1] <= 5:
                    w_Array_0[index] = 0
    
    #Closing
    w_Array_0 = ndimage.binary_closing(w_Array_0 , structure=np.ones((3,3))).astype(w_Array_0.dtype)
    #Filling
    w_Array_0 = ndimage.binary_fill_holes(w_Array_0 , structure=np.ones((3,3))).astype(w_Array_0.dtype)
    
    er_w_Array = ndimage.binary_erosion(w_Array_0).astype(w_Array_0.dtype)
    bound_w_Array = np.where((w_Array_0 == 1) & (er_w_Array == 0), 1, 0)
    
    Lab_w_Array, num_label = ndimage.label(w_Array_0 , structure = np.ones((3 , 3)))
    labels = np.arange(1, num_label + 1)
    area_label = ndimage.labeled_comprehension(w_Array_0, Lab_w_Array, labels, np.sum, float, 0)
    per_label = ndimage.labeled_comprehension(bound_w_Array, Lab_w_Array, labels, np.sum, float, 0)
    int_label = ndimage.labeled_comprehension(int_Array, Lab_w_Array, labels, np.mean, float, 0)
    len_label = np.zeros_like(area_label)
    wid_label = np.zeros_like(area_label)
    for i in range (0 , len(area_label)):
        temp_root = np.roots([1 , -1 * per_label[i] / 2 , area_label[i]])
        len_label[i] = np.max(temp_root)
        wid_label[i] = np.min(temp_root)

    num_label = len(len_label)

    obs = np.zeros((num_label , 2))
    obs[: , 0] = int_label
    obs[: , 1] = len_label
    
    w_obs = whiten(obs)

    cent , _ = kmeans(w_obs , 3)
    clu_label , _ = vq(w_obs , cent)


    area = np.zeros_like(int_Array)
    per = np.zeros_like(int_Array)
    width = np.zeros_like(int_Array)
    length = np.zeros_like(int_Array)
    inten = np.zeros_like(int_Array)
    clu = np.zeros_like(int_Array)
    for index , value in np.ndenumerate(Lab_w_Array):
        if value > 1:
            clu[index] = clu_label[int(value) - 1] + 1
            area[index] = area_label[int(value) - 1]
            per[index] = per_label[int(value) - 1]
            length[index] = len_label[int(value) - 1]
            width[index] = wid_label[int(value) - 1]
            inten[index] = int_label[int(value) - 1] + 1


    arcpy.NumPyArrayToRaster(area , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/area.tif')
    arcpy.NumPyArrayToRaster(per , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/per.tif')
    arcpy.NumPyArrayToRaster(width , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/width.tif')
    arcpy.NumPyArrayToRaster(length , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/length.tif')
    arcpy.NumPyArrayToRaster(inten , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/int_ave.tif')
    arcpy.NumPyArrayToRaster(clu , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/cluster.tif')





    
    #max_ele_label = ndimage.labeled_comprehension(ele_Array, Lab_w_Array, labels, np.max, float, 0)
    #min_ele_label = ndimage.labeled_comprehension(ele_Array, Lab_w_Array, labels, np.min, float, 0)
    


    

####    #d_ele_label = (max_ele_label - min_ele_label)
####    cir_label = (area_label / per_label) ** 0.5 * 2 * np.pi / per_label
####    len_wid_label = len_label / wid_label
##    
##    #d_ele_label = d_ele_label[area_label >= 10]
##    #cir_label = cir_label[area_label >= 10]
##    #len_wid_label = len_wid_label[area_label >= 10]
##    #labels = labels[area_label >= 10]
##
##    obs = np.zeros((num_label , 2))
##    obs[: , 0] = cir_label#.reshape(num_label , 1)
##    #obs[: , 1] = d_ele_label#.reshape(num_label , 1)
##    obs[: , 1] = len_wid_label#.reshape(num_label , 1)
##    w_obs = whiten(obs)
##
##    cent , _ = kmeans(w_obs , 3)
##    clu_label , _ = vq(w_obs , cent)
##
####    for i in range (0 , num_label):
####        print area_label[i] , cir_label[i] , d_ele_label[i] , len_wid_label[i] , clu_label[i]
##    
##    area = np.zeros_like(int_Array)
##    per = np.zeros_like(int_Array)
##    width = np.zeros_like(int_Array)
##    length = np.zeros_like(int_Array)
##    len_wid = np.zeros_like(int_Array)
##    #d_ele = np.zeros_like(int_Array)
##    clu = np.zeros_like(int_Array)
##    for index , value in np.ndenumerate(Lab_w_Array):
##        if value > 1:
##            area[index] = area_label[int(value) - 1]
##            per[index] = per_label[int(value) - 1]
##            length[index] = len_label[int(value) - 1]
##            width[index] = wid_label[int(value) - 1]
##            #d_ele[index] = d_ele_label[int(value) - 1]
##            clu[index] = clu_label[int(value) - 1] + 1
##            len_wid[index] = len_wid_label[int(value) - 1] + 1
##
##    cir = (area / per) ** 0.5 * 2 * np.pi / per
##
##    
##
##    arcpy.NumPyArrayToRaster(w_Array_0 , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/wet_0.tif')
##    arcpy.NumPyArrayToRaster(er_w_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/er_wet_0.tif')
##    arcpy.NumPyArrayToRaster(bound_w_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/bound_wet_0.tif')
##    arcpy.NumPyArrayToRaster(Lab_w_Array , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/blocks.tif')
##
##    arcpy.NumPyArrayToRaster(area , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/area.tif')
##    arcpy.NumPyArrayToRaster(per , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/per.tif')
##    arcpy.NumPyArrayToRaster(cir , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/cir.tif')
##    arcpy.NumPyArrayToRaster(width , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/width.tif')
##    arcpy.NumPyArrayToRaster(length , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/length.tif')
##    #arcpy.NumPyArrayToRaster(d_ele , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/d_ele.tif')
##    arcpy.NumPyArrayToRaster(len_wid , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/len_wid.tif')
##
##    arcpy.NumPyArrayToRaster(clu , corner,dx,dx , value_to_nodata=0).save('%s'%out_file_path +  '/cluster.tif')

           

