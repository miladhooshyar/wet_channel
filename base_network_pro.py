import arcpy, mmap, os, sys, shutil, traceback
from arcpy import env
from arcpy.sa import *
import numpy as np
import math
import base_network_fun
import scipy
from scipy import sparse
import itertools
import os
import matplotlib.pyplot as pl

################################################################################
################################################################################
def find_head(in_stream_Array , dir_Array):

    number_row = dir_Array.shape[0]
    number_col = dir_Array.shape[1]
    heads_list = [] 

    cx = scipy.sparse.coo_matrix(in_stream_Array)
    
    for ii , jj , v in zip(cx.row, cx.col, cx.data):
        #print ii , jj , v
        flage_no_head = 0
        for i in range (-1 , 2):
            for j in range (-1 , 2):
                new_i = ii + i 
                new_j = jj + j
                if bond_check(new_i , 0 , number_row) == 1 and bond_check(new_j , 0 , number_col):
                    if in_stream_Array[new_i , new_j] == 1:
                        move = dir_Array[new_i , new_j]
                        if bond_check(move , 1 , 129) == 1:
                            next_i, next_j = base_network_fun.move_cal(move, new_i , new_j)
                            if (next_i, next_j) == (ii , jj):
                                flage_no_head = 1

        if flage_no_head == 0:
            heads_list.append((ii , jj))
            
    #del in_stream_Array , dir_Array , cx
    
    return heads_list

#####################################################################################
#####################################################################################

def find_end(in_stream_Array , heads_list , dir_Array):
    
    ends_list = []
    num_row = dir_Array.shape[0]
    num_col = dir_Array.shape[1]

    for point in heads_list:
        new_i = point[0]
        new_j = point[1]
        last_point = point
        flage_end = 0
        while flage_end == 0:
            move = dir_Array[new_i , new_j]
            new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)
            if bond_check(new_i , 0 , num_row) == 0 or bond_check(new_j , 0 , num_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
                flage_end = 1
            elif in_stream_Array[new_i , new_j] != 1:
                flage_end = 1
                if (any((p == (new_i , new_j)) for p in ends_list)) == False:
                    ends_list.append((new_i , new_j))
            else:
                last_point = (new_i , new_j)
                    
    #del in_stream_Array , dir_Array , heads_list , 
    return ends_list

#############################################################################################################
#############################################################################################################

def find_head_block(in_stream_Array , order_Array , dir_Array):
    number_row = dir_Array.shape[0]
    number_col = dir_Array.shape[1]
    heads_list = [] 

    cx = scipy.sparse.coo_matrix(in_stream_Array)

    for ii , jj , v in zip(cx.row, cx.col, cx.data):
        #print ii , jj , v
        flage_no_head = 0
        order = order_Array[ii , jj]
        for i in range (-1 , 2):
            for j in range (-1 , 2):
                new_i = ii + i 
                new_j = jj + j
                if bond_check(new_i , 0 , number_row) == 1 and bond_check(new_j , 0 , number_col):
                    if in_stream_Array[new_i , new_j] == 1 and order_Array[new_i , new_j] == order:
                        move = dir_Array[new_i , new_j]
                        if bond_check(move , 1 , 129) == 1:
                            next_i, next_j = base_network_fun.move_cal(move, new_i , new_j)
                            if (next_i, next_j) == (ii , jj):
                                flage_no_head = 1

        if flage_no_head == 0:
            heads_list.append((ii , jj))
            
    #del in_stream_Array , dir_Array , cx
    
    return heads_list

#####################################################################################
#####################################################################################

def wet_transition(in_file_path , out_file_path, stream_raster , wet_stream_raster , flow_dir_raster , out_raster , loc_len , wet_ratio):
    total_num_add = 0

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
    WET_STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%wet_stream_raster

 
    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    wet_stream_Array = arcpy.RasterToNumPyArray(WET_STREAM_RASTER , nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth
                   
    
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    
    

    heads_list = find_head(Sp_stream_Array , dir_Array)

##    head_Array = np.zeros_like(stream_Array)   
##    p = 1
##    for index in heads_list:
##        head_Array[index] = p
##        p = p + 1
##    heads_raster = arcpy.NumPyArrayToRaster(head_Array,corner, dx ,dx)
##    heads_raster.save('%s'%out_file_path +  '/heads.tif')

    con_wet_stream = np.zeros_like(stream_Array)

    num_row = stream_Array.shape[0]
    num_col = stream_Array.shape[1]
    
    p = -1
    for index in heads_list:
        p = p + 1
        print p
        #if p == 332:
        list_passed , last_point , length = move_forward_connect_2(index , stream_Array , dir_Array , num_row , num_col)
        i  = 0
        for point in list_passed:
            len_wet_loc = 0
            i = i + 1
            for loc_i in range (i , min(i + loc_len , length)):
                if wet_stream_Array[list_passed[loc_i]] == 1:
                    len_wet_loc = len_wet_loc + 1
            
            act_loc_len = min(i + loc_len , length) - i

            if act_loc_len > 0:
                ratio_loc = float(len_wet_loc) / float(act_loc_len)
            else:
                ratio_loc = -1

            if ratio_loc >= wet_ratio:
                for loc_i in range (i , min(i + loc_len , length)):
                    con_wet_stream[list_passed[loc_i]] = 1
                
    heads_list = find_head(con_wet_stream , dir_Array)

    p = -1
    loc_len = 20
    for index in heads_list:
        p = p + 1
        print p
        #if p == 332:
        list_passed , last_point , length = move_forward_connect_2(index , con_wet_stream , dir_Array , num_row , num_col)
        i  = 0
        for point in list_passed:
            len_wet_loc = 0
            i = i + 1
            for loc_i in range (i , min(i + loc_len , length)):
                if wet_stream_Array[list_passed[loc_i]] == 1:
                    len_wet_loc = len_wet_loc + 1
            
            act_loc_len = min(i + loc_len , length) - i

            if act_loc_len > 0:
                ratio_loc = float(len_wet_loc) / float(act_loc_len)
            else:
                ratio_loc = -1

            if ratio_loc < 0.95:
                con_wet_stream[point] = 0
            else:
                break
                
    con_wet_raster = arcpy.NumPyArrayToRaster(con_wet_stream,corner, dx ,dx)
    con_wet_raster.save('%s'%out_file_path +  '/' + '%s'%out_raster)

    
#####################################################################################
#####################################################################################

def connect_stream_smart(in_file_path , out_file_path, stream_raster , flow_dir_raster , out_raster , connect_ratio):
    total_num_add = 0

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster

 
    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth
                   
    
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    
    

    heads_list = find_head(Sp_stream_Array , dir_Array)

##    head_Array = np.zeros_like(stream_Array)   
##    p = 1
##    for index in heads_list:
##        head_Array[index] = p
##        p = p + 1
##    heads_raster = arcpy.NumPyArrayToRaster(head_Array,corner, dx ,dx)
##    heads_raster.save('%s'%out_file_path +  '/heads.tif')

    num_row = stream_Array.shape[0]
    num_col = stream_Array.shape[1]

    list_end = []
    num_end = np.zeros_like(stream_Array)
    visited = np.zeros_like(stream_Array)

    p = -1
    for index in heads_list:
        p = p + 1
        #print p
        list_passed , last_point , length , visited = move_forward_connect_1(index , stream_Array , dir_Array , visited , num_row , num_col)
        #list_passed , last_point , length = move_forward_connect_2(index , stream_Array , dir_Array , num_row , num_col)
        if (any((p == last_point) for p in list_end)) == False:
            list_end.append(last_point)
        num_end[last_point] = num_end[last_point] + length

    #arcpy.NumPyArrayToRaster(num_end ,corner,dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/len.tif')
    num_add = 1
    while num_add > 0:
        num_add = 0
        for index in list_end:
            list_passed_gap , last_point_gap , length_gap = move_forward_gap(index , stream_Array , dir_Array , num_row , num_col)
            max_gap = num_end[index] * connect_ratio
                
            if length_gap > 0 and length_gap <= max_gap:
                for point in list_passed_gap:
                    if stream_Array[point] == 0:
                        stream_Array[point] = 1
                        num_add = num_add + 1
                        total_num_add = total_num_add + 1
                _ , last_point , _ = move_forward_connect_2(index , stream_Array , dir_Array , num_row , num_col)
                num_end[last_point] = num_end[last_point] + length_gap + num_end[index]
                list_end.remove(index)
                
    arcpy.NumPyArrayToRaster(stream_Array,corner,dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/' + '%s'%out_raster)
            

    
    
    return total_num_add

#####################################################################################
#####################################################################################

def move_forward_gap(index , Sp_stream_Array , dir_Array , number_row , number_col): 
    length = 1
    list_passed = []
    new_i = index[0]
    new_j = index[1]
    last_point = index
    flage_end = 0
    while flage_end == 0:
        list_passed.append((new_i , new_j))
        move = dir_Array[new_i][new_j]
        new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

        if bond_check(new_i , 0 , number_row) == 0 or bond_check(new_j , 0 , number_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
            #last_point = (new_i , new_j)
            flage_end = 1
        elif Sp_stream_Array[new_i , new_j] != 0:
            #last_point = (new_i , new_j)
            flage_end = 1
        else:
            last_point = (new_i , new_j)
            length = length + 1    
    return list_passed , last_point , length

def move_forward_connect_1(index , Sp_stream_Array , dir_Array , visited , number_row , number_col): 
    length = 1
    list_passed = []
    new_i = index[0]
    new_j = index[1]
    last_point = index
    flage_end = 0
    while flage_end == 0:
        list_passed.append((new_i , new_j))
        move = dir_Array[new_i][new_j]
        new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

        if bond_check(new_i , 0 , number_row) == 0 or bond_check(new_j , 0 , number_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
            #last_point = (new_i , new_j)
            flage_end = 1
        elif Sp_stream_Array[new_i , new_j] != 1:
            #last_point = (new_i , new_j)
            flage_end = 1
        else:
            last_point = (new_i , new_j)
            if visited[new_i , new_j] == 0:
                length = length + 1
            visited[new_i , new_j] = 1
    return list_passed , last_point , length , visited

def move_forward_connect_2(index , Sp_stream_Array , dir_Array , number_row , number_col): 
    length = 1
    list_passed = []
    new_i = index[0]
    new_j = index[1]
    last_point = index
    flage_end = 0
    while flage_end == 0:
        list_passed.append((new_i , new_j))
        move = dir_Array[new_i][new_j]
        new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

        if bond_check(new_i , 0 , number_row) == 0 or bond_check(new_j , 0 , number_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
            #last_point = (new_i , new_j)
            flage_end = 1
        elif Sp_stream_Array[new_i , new_j] != 1:
            #last_point = (new_i , new_j)
            flage_end = 1
        else:
            last_point = (new_i , new_j)
            length = length + 1    
    return list_passed , last_point , length

##########################################################################################################
##########################################################################################################
def bond_check(i , min_i , max_i):
    flage = 0
    if i >= min_i and i < max_i:
        flage = 1
    return flage
    
###############################################################################################################
###############################################################################################################     

def delet_isolated_stream(in_file_path , out_file_path, stream_raster , order_raster , flow_dir_raster , min_length_iso , out_raster):
    
    num_deleted = 0

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster


    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value = 0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value = 0)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth
            
    Sp_stream_Array = sparse.csr_matrix(stream_Array)

    number_row = stream_Array.shape[0]
    number_col = stream_Array.shape[1]

    heads_list = find_head(Sp_stream_Array , dir_Array)

    
    for index in heads_list:
        list_passed , last_point , flage_connected_end , ave_curve , length = move_forward(index , Sp_stream_Array , dir_Array , dir_Array , order_Array , number_row , number_col)
        if flage_connected_end == 0 and length <= min_length_iso:
            for point in list_passed:
                stream_Array[point] = 0
                num_deleted = num_deleted + 1

    arcpy.NumPyArrayToRaster(stream_Array,corner,dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/' + '%s'%out_raster)
       
    return num_deleted

########################################################################################
########################################################################################

def move_backward(dir_Array , Sp_stream_Array , ele_Array  , order_Array, list_passed , last_point , number_row , number_col , ele_thresh , length_thresh , head):
    alt_stream_list = [[]]
    #num_alt_st = 1
    alt_st = 0
    list_passed_new = []
    flage_done = 0
    list_passed_temp = []
    length_list = []
    while flage_done == 0:
        if alt_st > 0:
            alt_stream_list. append([])
        flage_done = 1
        flage_out = 0
        position = last_point

        length = 0

        temp_ele_thresh = ele_thresh
        temp_length_thresh = length_thresh

##        if head == 299:
##            print '000'
        while flage_out == 0:
            conut_found = 0
            for i in range (-1 , 2):
                for j in range (-1 , 2):
                    new_i = position[0] + i 
                    new_j = position[1] + j
                    if  bond_check(new_i , 0 , number_row) == 1 and bond_check(new_j , 0 , number_col) == 1:
                        move = dir_Array[new_i , new_j]
                        if bond_check(move , 1 , 129) == 1:
                            next_i, next_j = base_network_fun.move_cal(move, new_i , new_j)
                            if Sp_stream_Array[new_i , new_j] == 1 and (next_i, next_j) == position and \
                               (any((p == (new_i , new_j)) for p in list_passed)) == False and (any((p == (new_i , new_j)) for p in list_passed_new)) == False:
                                conut_found = conut_found + 1
                                next_position = (new_i , new_j)
                                if order_Array[next_position] == 1 and length == 1:
                                    temp_ele_thresh = 10 ** 10
                                    temp_length_thresh = 10 ** 10
                                elif order_Array[next_position] != 1:
                                    temp_ele_thresh = ele_thresh
                                    temp_length_thresh = length_thresh
                                
                            
            if conut_found > 1:
                #num_alt_st = num_alt_st + 1
                flage_done = 0
                list_passed_temp = []
                list_passed_temp.append(next_position)
                
##            if head == 138:
##                    print conut_found , temp_ele_thresh , ele_Array[next_position]

            if conut_found > 0 and ele_Array[position] <= temp_ele_thresh and length <= temp_length_thresh :          
                position = next_position
                alt_stream_list[alt_st].append(position)
                list_passed_temp.append(position)
                length = length + 1
            else:
                flage_out = 1
                length_list.append(length)
                
                alt_st = alt_st + 1
                for p in list_passed_temp:
                    list_passed_new.append(p)
    return alt_stream_list , len(alt_stream_list) , length_list

########################################################################################
########################################################################################
            
def move_forward(index , Sp_stream_Array , dir_Array , curve_Array , order_Array , number_row , number_col): 
    length = 1
    list_passed = []
    new_i = index[0]
    new_j = index[1]
    last_point = index
    flage_end = 0
    ave_curve = 0
    flage_connected_end = 0
    while flage_end == 0:
        list_passed.append((new_i , new_j))
        ave_curve = ave_curve + curve_Array[new_i , new_j]
        move = dir_Array[new_i][new_j]
        new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

        if bond_check(new_i , 0 , number_row) == 0 or bond_check(new_j , 0 , number_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
            #last_point = (new_i , new_j)
            flage_end = 1
        elif Sp_stream_Array[new_i , new_j] != 1:
            #last_point = (new_i , new_j)
            flage_end = 1
        elif order_Array[new_i , new_j] > 1:
            last_point = (new_i , new_j)
            flage_end = 1
            flage_connected_end = 1
        else:
            last_point = (new_i , new_j)
            length = length + 1
            
    return list_passed , last_point , flage_connected_end , (ave_curve / length) , length

########################################################################################
########################################################################################
def stream_delet_small_fast(in_file_path , out_file_path , bond_raster , stream_raster , dem_raster , flow_dir_raster , acc_raster , order_raster , curve_raster , ridge_curve_raster ,valley_curve_raster , out_raster , same_ratio , sub , step , deb_mode):

    num_deleted = 0

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ACC_RASTER = '%s'%in_file_path +  '/' + '%s'%acc_raster
    DEM_RASTER = '%s'%in_file_path +  '/' + '%s'%dem_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%curve_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
    BOND_RASTER = '%s'%in_file_path +  '/' + '%s'%bond_raster
    RIDGE_CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%ridge_curve_raster
    VALLEY_CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%valley_curve_raster

    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value=0)
    acc_Array = arcpy.RasterToNumPyArray(ACC_RASTER , nodata_to_value=0)
    ele_Array = arcpy.RasterToNumPyArray(DEM_RASTER , nodata_to_value=0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value=0)
    curve_Array = arcpy.RasterToNumPyArray(CURVE_RASTER , nodata_to_value=0)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    neg_bond =  arcpy.RasterToNumPyArray(BOND_RASTER , nodata_to_value=0)
    ridge_curve = arcpy.RasterToNumPyArray(RIDGE_CURVE_RASTER , nodata_to_value=0)
    valley_curve = arcpy.RasterToNumPyArray(VALLEY_CURVE_RASTER , nodata_to_value=0)

    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth
        
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    
    
    number_row = dir_Array.shape[0]
    number_col = dir_Array.shape[1]

    heads_list = find_head(Sp_stream_Array , dir_Array)

##    head_Array = np.zeros_like(stream_Array)   
##    p = 1
##    for index in heads_list:
##        head_Array[index] = p
##        p = p + 1
##    heads_raster = arcpy.NumPyArrayToRaster(head_Array,corner, dx ,dx)
##    heads_raster.save('%s'%out_file_path +  '/heads.tif')



    print len(heads_list)

    p = 0
    for index in heads_list:
        p = p + 1
        if stream_Array[index] == 1:
            flage_delet = 0
            list_passed , last_point , flage_connected_end , ave_curve , length = move_forward(index , Sp_stream_Array , dir_Array , curve_Array , order_Array , number_row , number_col)
            if flage_connected_end == 1:
                alt_stream_list , num_alt_st , alt_length_list = move_backward(dir_Array , Sp_stream_Array , ele_Array , order_Array , list_passed , last_point , number_row , number_col , (ele_Array[index] + 20) , (1.25 * length)  , p)

                
##                if p == 173:
##                    temp_st = np.zeros_like(curve_Array)
##                    for alt_st in range (0 , num_alt_st):
##                        for point in alt_stream_list[alt_st]:
##                            temp_st[point] = curve_Array[point]
##                    temp_st_raster = arcpy.NumPyArrayToRaster(temp_st,corner, dx ,dx)
##                    temp_st_raster.save('%s'%out_file_path +  '/temp_st.tif')
##
##                    temp_st = np.zeros_like(curve_Array)
##                    for point in list_passed:
##                        temp_st[point] = curve_Array[point]
##                    temp_st_raster = arcpy.NumPyArrayToRaster(temp_st,corner, dx ,dx)
##                    temp_st_raster.save('%s'%out_file_path +  '/temp_st_o.tif')
                
                #print 'Head = ' , p  , num_alt_st
                
                for alt_st in range (0 , num_alt_st):
                    curve_alt = 0
                    count_same = 0
                    list_dual_point = []
                    conuter_ave_curve = 0
                    if flage_delet == 0:
                        for point in list_passed:
                            min_diff = 100
                            for alt_point in alt_stream_list[alt_st]:
                                ele_diff = abs(ele_Array[point]- ele_Array[alt_point])
                                #print point , alt_point #ele_diff , min_diff
                                if ele_diff <= min_diff:
                                    dual_point = alt_point
                                    min_diff = ele_diff
                            if min_diff <= 100:
                                d_i = dual_point[0] - point[0]
                                d_j = dual_point[1] - point[1]
                                number_out_band = 0
                                temp_neg_bond = np.copy(neg_bond) 
                                for dt in range (0 , 30):
                                    new_i = int(point[0] + round(float(dt) / 29 * float(d_i)))
                                    new_j = int(point[1] + round(float(dt) / 29 * float(d_j)))
                                    mid_point = (new_i , new_j)
                                    if temp_neg_bond[mid_point] != 1: #and abs(ridge_curve[mid_point]) >= 0.5 * valley_curve[point]:
                                        number_out_band = number_out_band + 1
                                        temp_neg_bond[mid_point] = 1
                                        

                                if number_out_band <= 1: 
                                    count_same = count_same + 1
                                    if (any((pp == (dual_point)) for pp in list_dual_point)) == False:
                                        curve_alt = curve_alt + curve_Array[dual_point]
                                        list_dual_point.append(dual_point)
                                        conuter_ave_curve = conuter_ave_curve + 1
                                
                        flage_delete_stream = 0
                        flage_delete_alt_stream = 0
                        if count_same >= same_ratio * length:
                            
##                            print alt_stream_list[alt_st]
##                            print length , len(list_passed) , alt_length_list[alt_st] , len(alt_stream_list[alt_st]) 
                            com_lenght = min(length , alt_length_list[alt_st])

                            ave_curve = 0
                            ave_curve_alt = 0
                            for i in range(0 , com_lenght):
                                i_1 = length - 1 - i
                                ave_curve = ave_curve + curve_Array[list_passed[i_1]] / com_lenght
                                i_2 = i
                                ave_curve_alt = ave_curve_alt + curve_Array[alt_stream_list[alt_st][i_2]] / com_lenght
                                order_st = order_Array[list_passed[i_1]]
                                order_alt_st =order_Array[alt_stream_list[alt_st][i_2]]
##                                if p == 173:
##                                    print curve_Array[list_passed[i_1]] , curve_Array[alt_stream_list[alt_st][i_2]] , order_alt_st , order_st

                            
                            #curve_alt = curve_alt / conuter_ave_curve
                            
                            # higher order wins
                            if order_alt_st > order_st:
                                flage_delete_stream = 1
                            # they are both first order
                            else:
                                # if one is extramly smaller than the other
                                if length <= 0.5 * alt_length_list[alt_st]:
                                    flage_delete_stream = 1
                                elif alt_length_list[alt_st] <= 0.5 * length:
                                    flage_delete_alt_stream = 1
                                #they are in same size
                                elif ave_curve < ave_curve_alt:
                                    flage_delete_stream = 1
                                else:
                                    flage_delete_alt_stream = 1

##                        if p ==201 or p == 191:
##                            print p , ave_curve , ave_curve_alt , order_st , order_alt_st  , flage_delete_stream ,  flage_delete_alt_stream
                                    
                        if flage_delete_stream == 1:      
                            for point in list_passed:
                                stream_Array[point] = 0
                                num_deleted = num_deleted + 1
                            flage_delet = 1
                            
                        if flage_delete_alt_stream == 1:      
                            for point in alt_stream_list[alt_st]:
                                if order_Array[point] == 1:
                                    stream_Array[point] = 0
                                    num_deleted = num_deleted + 1
                                
    arcpy.NumPyArrayToRaster(stream_Array,corner,dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/' + '%s'%out_raster)
    
    return num_deleted                                  

        

        
        
        
########################################################################################
########################################################################################

def stream_extention(in_file_path , out_file_path  , stream_raster , old_stream_raster , dem_raster , flow_dir_raster , acc_raster , order_raster , old_order_raster , curve_raster , out_raster , sub , step , deb_mode):

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ACC_RASTER = '%s'%in_file_path +  '/' + '%s'%acc_raster
    DEM_RASTER = '%s'%in_file_path +  '/' + '%s'%dem_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    OLD_ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%old_order_raster
    CURVE_RASTER = '%s'%in_file_path +  '/' + '%s'%curve_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
    OLD_STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%old_stream_raster


    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value=0)
    acc_Array = arcpy.RasterToNumPyArray(ACC_RASTER , nodata_to_value=0)
    ele_Array = arcpy.RasterToNumPyArray(DEM_RASTER , nodata_to_value=0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value=0)
    old_order_Array = arcpy.RasterToNumPyArray(OLD_ORDER_RASTER , nodata_to_value=0)
    curve_Array = arcpy.RasterToNumPyArray(CURVE_RASTER , nodata_to_value=0)
    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    old_stream_Array = arcpy.RasterToNumPyArray(OLD_STREAM_RASTER , nodata_to_value=0)

    corner = arcpy.Point(arcpy.Describe(DIR_RASTER).Extent.XMin,arcpy.Describe(DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(DIR_RASTER).meanCellWidth
        
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    old_Sp_stream_Array = sparse.csr_matrix(old_stream_Array)
    
    
    number_row = dir_Array.shape[0]
    number_col = dir_Array.shape[1]

    heads_list = find_head(Sp_stream_Array , dir_Array)

    for index in heads_list:
        list_passed , last_point , flage_connected_end , ave_curve , length = move_forward(index , Sp_stream_Array , dir_Array , curve_Array , order_Array , number_row , number_col)
        if flage_connected_end == 1:
            alt_stream_list , num_alt_st , alt_length_list = move_backward(dir_Array , old_Sp_stream_Array , ele_Array , old_order_Array , list_passed , index , number_row , number_col , (ele_Array[index] + 1) , (1.25 * length)  , 0)
            if num_alt_st > 0:
                max_curve = 0
                for alt_st in range (0 , num_alt_st):
                    if alt_length_list[alt_st] > 1:
                        count = 0
                        ave_curve = 0
                        for point in alt_stream_list[alt_st]:
                            ave_curve = ave_curve + curve_Array[point]
                            count = count + 1
                        ave_curve = ave_curve / count

                        if ave_curve > max_curve:
                            best_alt_st = alt_st
                            max_curve =  ave_curve
                if  max_curve > 0:       
                    for point in alt_stream_list[best_alt_st]:
                        stream_Array[point] = 1
                    
    arcpy.NumPyArrayToRaster(stream_Array , corner , dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/' + '%s'%out_raster)
           
####################################################################################################################
####################################################################################################################

def find_por_point(in_file_path , out_file_path , stream_raster , dem_raster ,  flow_dir_raster , acc_raster  , order_raster , add_elevation , out_text):

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    DEM_RASTER = '%s'%in_file_path +  '/' + '%s'%dem_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster
    ACC_RASTER = '%s'%in_file_path +  '/' + '%s'%acc_raster
    

    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value=0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value=0)
    dem_Array = arcpy.RasterToNumPyArray(DEM_RASTER , nodata_to_value=0)
    acc_Array = arcpy.RasterToNumPyArray(ACC_RASTER , nodata_to_value=0)
    
    dsc=arcpy.Describe(DIR_RASTER)
    ext=dsc.Extent
    corner=arcpy.Point(ext.XMin,ext.YMin)
    
    X_min = dsc.EXTENT.XMin
    Y_min = dsc.EXTENT.YMin

    X_max = dsc.EXTENT.XMax
    Y_max = dsc.EXTENT.YMax

    dy = dsc.meanCellHeight
    dx = dsc.meanCellWidth
        
        
    #Step 1: Find channel heads and elevation
    heads_list = find_head(Sp_stream_Array , dir_Array)
    num_row = dir_Array.shape[0]
    num_col = dir_Array.shape[1]
    
    channel_head = []
    channel_ele = []
    for index in heads_list:
        channel_head.append(index)
        channel_ele.append(dem_Array[index])
    
    
    #Step 2: find por points
    por_point = []
    por_ele = []
    p = -1
    for index in heads_list:
        p = p + 1
        new_i = index[0]
        new_j = index[1]
        flage_end = 0
        list_passed = []
        length = 0
        while flage_end == 0:
            list_passed.append((new_i , new_j))
            move = dir_Array[new_i , new_j]
            new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)
            
            if bond_check(new_i , 0 , num_row) == 0 or bond_check(new_j , 0 , num_col) == 0 or bond_check(move , 1 , 129) == 0 or bond_check(dir_Array[new_i , new_j] , 1 , 129) == 0:
                flage_end = 1
            elif order_Array[new_i , new_j] != 1: 
                last_point = (new_i , new_j)
                flage_end = 1
            else:
                length = length + 1
        
        por_point.append(list_passed[length - 2])
        por_ele.append(dem_Array[list_passed[length - 2]])

    file_por = open('%s' %out_file_path + '/por_point.txt','w')
    number_point = len(por_point)
    for p in range (0 , number_point):
        file_por.write('%f '%(X_min + por_point[p][1] * dx + dx / 2))
        file_por.write('% f '%(Y_max - por_point[p][0] * dy - dy / 2))
        file_por.write('% f '%(channel_ele[p] + add_elevation))
        file_por.write('% f '%(por_ele[p]))
        file_por.write('% f '%(X_min + channel_head[p][1] * dx + dx / 2))
        file_por.write('% f '%(Y_max - channel_head[p][0] * dy  - dy / 2))
        file_por.write('% d \n'%p)
    file_por.close()

########################################################################
########################################################################

def unchannel_delet(in_file_path , out_file_path , stream_raster ,  flow_dir_raster ,  order_raster , Chan_Unchan , Unch_thresh , out_raster):
    
    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster

    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value=0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value=0)
    
    dsc=arcpy.Describe(DIR_RASTER)
    ext=dsc.Extent
    corner=arcpy.Point(ext.XMin,ext.YMin)
    

    #Step 1: Find channel heads and elevation
    heads_list = find_head(Sp_stream_Array , dir_Array)
    num_row = dir_Array.shape[0]
    num_col = dir_Array.shape[1]
    
    
    #Step 2: find por points

    p = - 1

    num_delet = 0

    for index in heads_list:
        p = p + 1
        if Chan_Unchan[p , 2] < Unch_thresh:
            num_delet = num_delet + 1
            new_i = index[0]
            new_j = index[1]
            flage_end = 0
            list_passed = []
            length = 0
            while flage_end == 0:
                list_passed.append((new_i , new_j))
                                    
                move = dir_Array[new_i , new_j]
                new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

                if bond_check(new_i , 0 , num_row) == 0 or bond_check(new_j , 0 , num_col) == 0 or bond_check(move , 1 , 129) == 0:
                    flage_end = 1
                elif order_Array[new_i , new_j] != 1:
                    last_point = (new_i , new_j)
                    flage_end = 1
                else:
                    length = length + 1
            for point in list_passed:
                stream_Array[point] = 0
    

    cleaned_st_raster = arcpy.NumPyArrayToRaster(stream_Array , corner,dsc.meanCellWidth,dsc.meanCellHeight , value_to_nodata=0)
    cleaned_st_raster.save('%s'%out_file_path +  '/' + '%s'%out_raster)
    #cleaned_st_raster = None
            
    return num_delet

########################################################################################################################################################
########################################################################################################################################################

def head_delet(in_file_path , out_file_path , dem_raster , stream_raster ,  flow_dir_raster ,  order_raster , head_ele , out_raster):
    #env.workspace = in_file_path

    DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%flow_dir_raster
    ORDER_RASTER = '%s'%in_file_path +  '/' + '%s'%order_raster
    DEM_RASTER = '%s'%in_file_path +  '/' + '%s'%dem_raster
    STREAM_RASTER = '%s'%in_file_path +  '/' + '%s'%stream_raster

    stream_Array = arcpy.RasterToNumPyArray(STREAM_RASTER , nodata_to_value=0)
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    dir_Array = arcpy.RasterToNumPyArray(DIR_RASTER , nodata_to_value=0)
    order_Array = arcpy.RasterToNumPyArray(ORDER_RASTER , nodata_to_value=0)
    dem_Array = arcpy.RasterToNumPyArray(DEM_RASTER , nodata_to_value=0)
    
    dsc=arcpy.Describe(DIR_RASTER)
    ext=dsc.Extent
    corner=arcpy.Point(ext.XMin,ext.YMin)

    #Step 1: Find channel heads and elevation
    heads_list = find_head(Sp_stream_Array , dir_Array)
    num_row = dir_Array.shape[0]
    num_col = dir_Array.shape[1]
    
    
    #Step 2: find por points

    p = - 1

    for index in heads_list:
        p = p + 1
        new_i = index[0]
        new_j = index[1]
        flage_end = 0
        list_passed = []
        length = 0
        list_passed.append(index)
        if head_ele[p , 0] > 0:
            while flage_end == 0:
                move = dir_Array[new_i , new_j]
                new_i, new_j = base_network_fun.move_cal(move, new_i , new_j)

                if bond_check(new_i , 0 , num_row) == 0 or bond_check(new_j , 0 , num_col) == 0 or bond_check(move , 1 , 129) == 0:
                    flage_end = 1
                elif order_Array[new_i , new_j] != 1:
                    last_point = (new_i , new_j)
                    flage_end = 1
                else:
                    length = length + 1

                    if dem_Array[new_i , new_j] > head_ele[p , 0]:
                        list_passed.append((new_i , new_j))
                        
            for point in list_passed:
                stream_Array[point] = 0
    

    cleaned_st_raster = arcpy.NumPyArrayToRaster(stream_Array , corner,dsc.meanCellWidth,dsc.meanCellHeight , value_to_nodata=0)
    cleaned_st_raster.save('%s'%out_file_path +  '/' + '%s'%out_raster)

########################################################################################################################################################
########################################################################################################################################################

def find_por_point_simple(in_file_path , out_file_path , stream_raster ,  dir_raster , acc_raster , scale):
    env.workspace = in_file_path
    
    dsc=arcpy.Describe(stream_raster)
    sp_ref=dsc.SpatialReference
    ext=dsc.Extent
    corner=arcpy.Point(ext.XMin,ext.YMin)
    
    X_min = dsc.EXTENT.XMin
    Y_min = dsc.EXTENT.YMin

    X_max = dsc.EXTENT.XMax
    Y_max = dsc.EXTENT.YMax

    dy = dsc.meanCellHeight
    dx = dsc.meanCellWidth
    
    stream_Array = arcpy.RasterToNumPyArray(stream_raster , nodata_to_value=0)
    Sp_stream_Array = sparse.csr_matrix(stream_Array)
    
    dir_Array = arcpy.RasterToNumPyArray(dir_raster)
    acc_Array = arcpy.RasterToNumPyArray(acc_raster)

        

    #Step 1: Find channel heads and elevation
    heads_list = find_head(Sp_stream_Array , dir_Array)
    num_row = dir_Array.shape[0]
    num_col = dir_Array.shape[1]

    number_point = 0
    channel_head = []
    for point in heads_list:
        print 'SubBasin' , number_point , ',' , 'Area = ' , int(float(acc_Array[point]) * scale)
        channel_head.append((X_min + point[1] * dx + dx / 2 , Y_max - point[0] * dy  - dy / 2))
        number_point += 1


    return channel_head , number_point
            

########################################################################################################################################################
########################################################################################################################################################
    
def sub_DEM(map_file_path, text_file_path , por_point , flow_acc_raster , flow_dir_raster , dem_raster , st_counter , trail):

    #env.workspace = map_file_path
    flow_acc = Raster(flow_acc_raster)
    flow_dir = Raster(flow_dir_raster)
    raster = Raster(dem_raster)
    
    dsc = arcpy.Describe(raster)
    
    p = -1
    for point in por_point:
        p += 1
        out_name = 'por_' + str(p) + '.shp'
        arcpy.CreateFeatureclass_management(map_file_path, out_name, "POINT", "", "DISABLED", "DISABLED", dsc.SpatialReference)
        cursor=arcpy.da.InsertCursor(out_name, ["SHAPE@"])
        cursor.insertRow([arcpy.Point(float(point[0]),float(point[1]))])
        del cursor
        tolerance = 0
        outSnapPour = SnapPourPoint(out_name, flow_acc, tolerance)
        samll_basin = Watershed(flow_dir, outSnapPour)
        
        poly_bond =  '%s'%map_file_path  + '/'  +  'bond' + str(st_counter + p) + '.shp'
        buff_poly_bond =  '%s'%map_file_path  + '/'  +  'bond_buffer' + str(st_counter + p) + '.shp'
        
        arcpy.RasterToPolygon_conversion (samll_basin, poly_bond)
        arcpy.Buffer_analysis(poly_bond , buff_poly_bond, "50 Meters")
        
        samll_basin = Con(IsNull(samll_basin) , 0 , 1)

        if p == 0:
            big_basin = samll_basin
        else:
            
            big_basin =  samll_basin + big_basin
        
        arcpy.env.extent = buff_poly_bond

        subbasin = '%s'%map_file_path + '/subDEM_' + str(st_counter + p) + '.tif'
        arcpy.Clip_management(raster , "", subbasin, buff_poly_bond, "", "ClippingGeometry")
        
        #subbasin =  Con(samll_basin == 1, raster)
        
        arcpy.env.extent = dem_raster

        #subbasin.save('%s'%map_file_path + '/subDEM_' + str(st_counter + p) + '.tif')
        
        outSnapPour = None
        subbasin= None
        samll_basin = None
        poly_bond = None

        for the_file in os.listdir(map_file_path):
            if the_file.startswith("por"):
                file_path = os.path.join(map_file_path, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception, e:
                    print e
    
    #subbasin =  Con(big_basin == 0, raster)
    subdir =  Con(big_basin == 0, flow_dir_raster)

    #subbasin.save('%s'%map_file_path + '/DEM_' + str(trail) + '.tif')
    subdir.save('%s'%map_file_path + '/flow_dir_' + str(trail) + '.tif')
    
    

    big_basin = None
    
    for the_file in os.listdir(map_file_path):
        if the_file.startswith("por"):
            file_path = os.path.join(map_file_path, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception, e:
                print e , 1

##########################################################################################################
##########################################################################################################
                
def Subbasin_find(DEM_file_path , CDEM_file_path , file_raster , acc_tresh , cell_size_cal , cell_size_cut):
    scale = ((cell_size_cut / cell_size_cal) ** 2)
    acc_tresh = acc_tresh / scale
    base_network_fun.raster_resample(DEM_file_path, CDEM_file_path , cell_size_cal , file_raster , 'DEM.tif' , -1 , -1 , False)
    base_network_fun.raster_resample(CDEM_file_path, CDEM_file_path , cell_size_cut , 'DEM.tif' , 'DEM_0.tif' , -1 , -1 , False)
    base_network_fun.fill_dem(CDEM_file_path, CDEM_file_path , 'DEM_0.tif' , 'fill_0.tif' , -1 , -1 , False)
    base_network_fun.flow_dir(CDEM_file_path, CDEM_file_path , 'fill_0.tif' , 'flow_dir_0.tif' , -1 , -1 , False)
    
    total_subasin = 0
    trail = 0
    flage_done = 0
    while flage_done == 0:
        temp_acc_tresh = acc_tresh
        #in_raster = 'DEM_' + '%s'%str(int(trail)) + '.tif'
        fill_raster = 'fill_' + '%s'%str(int(trail)) + '.tif'
        flow_dir = 'flow_dir_' + '%s'%str(int(trail)) + '.tif'
        flow_acc = 'flow_acc_' + '%s'%str(int(trail)) + '.tif'
        stream = 'stream_' + '%s'%str(int(trail)) + '.tif'
        
        #base_network_fun.fill_dem(CDEM_file_path, CDEM_file_path , in_raster , fill_raster)
        #base_network_fun.flow_dir(CDEM_file_path, CDEM_file_path , fill_raster , flow_dir)
        base_network_fun.flow_acc(CDEM_file_path, CDEM_file_path , flow_dir , flow_acc , -1 , -1 , False)
        max_acc = base_network_fun.flow_acc_max(CDEM_file_path, CDEM_file_path,  flow_acc , -1 , -1 , False) 
        #print 'Trail = ' , trail , 'Max. Area = ' , max_acc


            
        if max_acc < acc_tresh:
            temp_acc_tresh = max_acc - 5

        if max_acc < acc_tresh * 0.1:
            flage_done = 1
        else:
            base_network_fun.flow_acc_bond(CDEM_file_path, CDEM_file_path , temp_acc_tresh , flow_acc, stream , -1 , -1 , False)
            por_point , number_por = find_por_point_simple(CDEM_file_path , CDEM_file_path  , stream , flow_dir , flow_acc , scale)

            sub_DEM(CDEM_file_path, CDEM_file_path , por_point ,  flow_acc , flow_dir , 'DEM.tif' , total_subasin , (trail + 1))

            total_subasin += number_por

            trail += 1


    for the_file in os.listdir(CDEM_file_path):
        if the_file.startswith("sub") == False and the_file.startswith("bond") == False:
            file_path = os.path.join(CDEM_file_path, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception, e:
                print e , 2
    return total_subasin

##################################################################################################################
##################################################################################################################
def stream_clip(in_file_path , out_file_path ,  bond_name , stream_raster ,  out_raster):

    STREAM_RASTER =  '%s'%in_file_path + '/' + '%s'%stream_raster
    Bond_Poly = '%s'%in_file_path +  '/' + '%s'%bond_name
    OUT_STREAM = '%s'%out_file_path + '/' +  '%s'%out_raster
    arcpy.Clip_management(STREAM_RASTER , "", OUT_STREAM , Bond_Poly, "", "ClippingGeometry")
               

##################################################################################################################
##################################################################################################################
def extraction(bond_name , in_raster ,  out_raster):
    arcpy.sa.ExtractByMask(in_raster, bond_name).save(out_raster)
    
##################################################################################################################
##################################################################################################################
def stream_merge(main_map_file_path , stream_raster , number_of_sub , out_raster , pix_type):
    stream_list = []
    for sub in range (0 , number_of_sub):
        map_file_path = '%s'%main_map_file_path  + '/'  +  str(sub)
        ind_stream = '%s'%map_file_path + '/' + '%s'%stream_raster
        stream_list.append(ind_stream)
        if sub == 0:
            dsc = arcpy.Describe(ind_stream)

    arcpy.MosaicToNewRaster_management(stream_list , main_map_file_path, out_raster, "", pix_type , "" , 1 , "MAXIMUM", "FIRST")    


#############################
def work_check(sub):
    ORG_DEM = 'C:/miladhooshyar/Spring2014/GIS/archydro_curve/Valley_Network/CDEM' + '/'+ 'subDEM_' +  str(sub) + '.tif'
    TT = True
    while TT == True:
        try:
            org_ext = arcpy.Describe(ORG_DEM).Extent.XMin
            env_path = env.workspace
            ext = env.extent
            sk_path = env.scratchWorkspace
            TT = False
        except:
            print 'Fail_Workspace'
            TT = True
    return os.path.basename(env_path) == str(sub) , org_ext == ext.XMin , os.path.basename(sk_path) == 'Temp' + str(sub) 
