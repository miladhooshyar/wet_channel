
# Milad Hooshyar, 1/19/2015#

import cv2
import matplotlib.pyplot as plt
import arcpy
import base_network_fun
import base_network_pro
import Penira_malik
import sys
import os
import numpy as np
import INT_PROS
import Prob_W
import Penira_malik


main_file_path = "C:/miladhooshyar/Spring2014/GIS/archydro_curve/WET"



in_file_path = '%s'%main_file_path	 + '/output/ward_2010_5m'


#Penira_malik.Penira_malik(in_file_path, in_file_path , 50 , 5 , 0.1,   'INT_NV_S.tif' , 'P_5_01_2.tif' , 2)

INT_RASTER = '%s'%in_file_path +  '/INT_NV_P.tif'
#VAL_RASTER = '%s'%in_file_path +  '/FVB.tif'
#base_network_fun.raster_focal_st(in_file_path, in_file_path, 3 , 1 , 'INT_NV_P.tif' , 'INT_NV_PF_15.tif' , "MINIMUM" , "NODATA")
INT_F_RASTER = '%s'%in_file_path +  '/INT_NV_F.tif'

corner = arcpy.Point(arcpy.Describe(INT_RASTER).Extent.XMin,arcpy.Describe(INT_RASTER).Extent.YMin)
dx = arcpy.Describe(INT_RASTER).meanCellWidth

#val_Array = arcpy.RasterToNumPyArray(VAL_RASTER  , nodata_to_value= 0)
int_f_Array = arcpy.RasterToNumPyArray(INT_F_RASTER , nodata_to_value= 0)
img = arcpy.RasterToNumPyArray(INT_RASTER  , nodata_to_value= 0)
img = np.uint8(img)
#img = cv2.imread('messi5.jpg',0)

img_1 = img[img > 0]
v = np.median(img_1)
sigma = 0.33
lower = int(max(0, (1.0 - sigma) * v))
upper = int(min(255, (1.0 + sigma) * v))

print lower , upper , v

edges = cv2.Canny(img , 60 , 80 , apertureSize = 3)

edges = edges + 1
#edges = np.where((edges == 0) , 1 , 0)

#edges = np.where((edges == 0) & (val_Array == 1) & (int_f_Array <= 114) & (int_f_Array > 0), 1 , 0)
edges = np.where((edges == 0) & (int_f_Array > 0), 1 , 0)

arcpy.NumPyArrayToRaster(edges , corner,dx,dx).save('%s'%in_file_path +  '/edges.tif')

#gray = cv2.cvtColor(edges , cv2.COLOR_BGR2GRAY)
#edges = cv2.Canny(gray,50,150,apertureSize = 3)


plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()


