import arcpy
from arcpy import env
import numpy as np
import scipy
import math
import os


def move_cal(m , i , j):
    if m == 1:
        new_i = i
        new_j = j + 1
    elif m == 2:
        new_i = i + 1
        new_j = j + 1
    elif m == 4:
        new_i = i + 1
        new_j = j 
    elif m == 8:
        new_i = i + 1
        new_j = j - 1
    elif m == 16:
        new_i = i
        new_j = j - 1
    elif m == 32:
        new_i = i - 1
        new_j = j - 1
    elif m == 64:
        new_i = i - 1
        new_j = j
    elif m == 128:
        new_i = i - 1
        new_j = j + 1
    else:
        new_i = i
        new_j = j
    return new_i, new_j


#########################################

def move_rev_cal(i , j):
    if i == 0 and j == 1:
        m = 1
    elif i == 1 and j == 1:
        m = 2
    elif i == 1 and j == 0:
        m = 4
    elif i == 1 and j == -1:
        m = 8
    elif i == 0 and j == -1:
        m = 16
    elif i == -1 and j == -1:
        m = 32
    elif i == -1 and j == 0:
        m = 64
    elif i == -1 and j == 1:
        m = 128
    else:
        m = 0 
    return m

    
##################################################################################
    ###########################################################################
def dir_fix(in_file_path, out_file_path , dem_raster , int_raster ,org_flow_dir_Raster , out_raster, int_tresh):

    INT_RASTER = '%s'%in_file_path +  '/' + '%s'%int_raster
    DEM_RASTER = '%s'%in_file_path +  '/' + '%s'%dem_raster
    ORG_DIR_RASTER = '%s'%in_file_path +  '/' + '%s'%org_flow_dir_Raster


    dem_Array = arcpy.RasterToNumPyArray(DEM_RASTER, nodata_to_value=0)
    int_Array = arcpy.RasterToNumPyArray(INT_RASTER, nodata_to_value=0)
    org_flow_dir_Array = arcpy.RasterToNumPyArray(ORG_DIR_RASTER, nodata_to_value=0)
    corner = arcpy.Point(arcpy.Describe(ORG_DIR_RASTER).Extent.XMin,arcpy.Describe(ORG_DIR_RASTER).Extent.YMin)
    dx = arcpy.Describe(ORG_DIR_RASTER).meanCellWidth       
        
    number_row = dem_Array.shape[0]
    number_col = dem_Array.shape[1]

    flow_dir = np.zeros((number_row , number_col) , dtype = np.uint8)

    for index , value in np.ndenumerate(dem_Array):
        min_int = 1000
        if value > 0 and value <= 5000:
            m_1 = 0
            m_2 = 0
            for i in range (-1 , 2):
                    for j in range (-1 , 2):
                        new_i = index[0] + i 
                        new_j = index[1] + j
                        if new_i >= 0 and new_i < number_row and new_j >= 0 and new_j < number_col and (i != 0 or j != 0):
                            if value >= dem_Array[new_i , new_j]:
                                if int_Array[new_i , new_j] < min_int:
                                    min_int = int_Array[new_i , new_j]
                                    m_1 = i
                                    m_2 = j
            flow_dir[index] = move_rev_cal(m_1 , m_2)
            
            if value == dem_Array[index[0] + m_1 , index[1] + m_2]: #flat area
                flow_dir[index] = org_flow_dir_Array[index]
            if min_int >= int_tresh: #noisy area
                flow_dir[index] = org_flow_dir_Array[index]

    arcpy.NumPyArrayToRaster(flow_dir,corner, dx , dx , value_to_nodata=0).save('%s'%out_file_path +  '/' + '%s'%out_raster)
            
