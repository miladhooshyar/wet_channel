
# Milad Hooshyar, 1/19/2015#

import matplotlib.pyplot as pl
import arcpy
import base_network_fun
import base_network_pro
import Penira_malik
import sys
import os
import numpy as np
import INT_PROS
import Prob_W


main_file_path = "C:/miladhooshyar/Spring2014/GIS/archydro_curve/WET"

cell_size = 0.5 # in DEM uint (ft or meter)
org_cell_size = 0.5
unit = 'm' # ft or m
buffer_size = 3
tolerance = 3
local_length = 50
diff_thresh = 2
wet_ratio = 0.75
connect_ratio = 1


input_file_path = '%s'%main_file_path  + '/Input/Trout_5m'
output_file_path = '%s'%main_file_path	 + '/output/Trout_5m'

ssss

os.makedirs(output_file_path)
os.makedirs(output_file_path + '/'  +	 'Temp')

arcpy.env.scratchWorkspace = output_file_path + '/'  +	 'Temp'
arcpy.env.workspace = output_file_path
arcpy.env.extent = '%s'%input_file_path	+ '/VB.tif'
arcpy.env.outputCoordinateSystem = '%s'%input_file_path	+ '/VB.tif'


base_network_fun.raster_copy(input_file_path, output_file_path, 'valley.tif' , 'valley.tif' )
base_network_fun.raster_copy(input_file_path, output_file_path, 'FdirC.tif' , 'FdirC.tif')
base_network_fun.raster_copy(input_file_path, output_file_path, 'FaccC.tif' , 'FaccC.tif')

base_network_pro.extraction(input_file_path + '/bond.shp' , input_file_path + '/INT.tif' ,  output_file_path + '/INT.tif')
base_network_pro.extraction(input_file_path + '/bond.shp' , input_file_path + '/DEM_a.tif' ,  output_file_path + '/DEM_a.tif')
base_network_pro.extraction(input_file_path + '/bond.shp' , input_file_path + '/DEM_g.tif' ,  output_file_path + '/DEM_g.tif')

##base_network_pro.stream_clip(input_file_path , output_file_path ,  'bond.shp' , 'INT.tif' ,  'INT.tif')
##base_network_pro.stream_clip(input_file_path , output_file_path ,  'bond.shp' , 'DEM_a.tif' ,  'DEM_a.tif')
##base_network_pro.stream_clip(input_file_path , output_file_path ,  'bond.shp' , 'DEM_g.tif' ,  'DEM_g.tif')

base_network_fun.valley_bond(input_file_path, output_file_path , 'VB.tif' , 'valley.tif' , buffer_size)

Prob_W.veg_bond(output_file_path , 'DEM_a.tif' , 'DEM_g.tif' , diff_thresh)
base_network_fun.cut_dem(output_file_path, output_file_path , 'INT.tif' , 'No_veg.tif' , 'INT_NV.tif')

Penira_malik.Penira_malik(output_file_path, output_file_path , 50 , 10 , 0.1,   'INT_NV.tif' , 'INT_NV_P.tif' , 2)

#base_network_fun.raster_focal_st(output_file_path, output_file_path, cell_size , org_cell_size , 'INT_NV.tif'  , 'INT_Fo.tif')
#base_network_fun.raster_resample(output_file_path, output_file_path, cell_size , 'INT_Fo.tif'  , 'INT_O.tif')

#base_network_fun.raster_resample(output_file_path, output_file_path, cell_size , 'FVB.tif'  , 'FVB_R.tif')
base_network_fun.raster_resample(output_file_path, output_file_path, org_cell_size , 'FVB.tif'  , 'FVB_R.tif')
base_network_fun.cut_dem(output_file_path, output_file_path , 'INT_NV_P.tif' , 'FVB_R.tif' , 'INT_C.tif')

print '1'
fitting_result = Prob_W.prob_fit(output_file_path , 'INT_NV.tif')
##ssss

Prob_W.edge_find(output_file_path, 'INT_NV_P.tif' , 'FVB_R.tif' , 60 , 80 , 21.085 , 112.97)

print '2'
#Prob_W.wet_bond_simple(output_file_path, output_file_path , 'INT_C.tif' , 93)
#Prob_W.wet_bond(output_file_path, output_file_path , 'INT_C.tif' , fitting_result)
print '3'
Prob_W.refine_wet(output_file_path, output_file_path , 'WMD.tif')
#base_network_fun.raster_focal_st(output_file_path, output_file_path, cell_size , org_cell_size , 'wet_bond.tif'  , 'wet_bond_Fo.tif')
#base_network_fun.raster_resample(output_file_path, output_file_path, cell_size , 'wet_bond_Fo.tif'  , 'wet_bond_Fo_R.tif')
print '4'
#Prob_W.make_stream(output_file_path , 'wet_bond.tif' , 'valley.tif' , 'FaccC.tif'  , tolerance)
Prob_W.make_stream_simple(output_file_path , 'wet_bond.tif' , 'valley.tif' , 'FdirC.tif'  , org_cell_size , tolerance)

print '5'
base_network_fun.stream_to_line(output_file_path, output_file_path, 'WS.tif' , 'FdirC.tif' , 'Wet_Stream.shp')
print '6'
base_network_pro.wet_transition(output_file_path , output_file_path, 'valley.tif' , 'WS.tif' , 'FdirC.tif' , 'CWS.tif' , local_length , wet_ratio)

print '7'
base_network_fun.stream_to_line(output_file_path, output_file_path, 'CWS.tif' , 'FdirC.tif' , 'CWS.shp')
print '8'

base_network_pro.connect_stream_smart(output_file_path , output_file_path, 'CWS.tif' , 'FdirC.tif' , 'Connected_WS.tif' , connect_ratio)
base_network_fun.stream_to_line(output_file_path, output_file_path, 'Connected_WS.tif' , 'FdirC.tif' , 'Connected_WS.shp')

#base_network_fun.cut_dem(map_file_path, map_file_path ,	  'INT_F.tif' ,	  'VB_R.tif' ,   'C_INT_F.tif' , 0 , 2 , False)
#base_network_fun.cut_dem(map_file_path, map_file_path ,	  'INT_F.tif' ,	  'RB.tif' ,   'CR_INT_F.tif' , 0 , 2 , False)


##INT_raster = arcpy.RasterToNumPyArray('C_INT_F.tif' , nodata_to_value=0)
##
##fig = pl.figure(4)
##
##bins = range(1 , np.max(INT_raster) , 2)
##hist , xx = np.histogram(INT_raster , bins)
##pl.plot(xx[0:xx.size - 1] , hist)

##INT_raster = arcpy.RasterToNumPyArray('C_INT_O.tif' , nodata_to_value=0)
##bins = range(1 , np.max(INT_raster) , 2)
##hist , xx = np.histogram(INT_raster , bins)
##pl.plot(xx[0:xx.size - 1] , hist)
##
##for i in range (0 , len(bins)):
##    print bins[i] , float(hist[i]) / np.sum(hist)

##INT_raster = arcpy.RasterToNumPyArray('INT_O.tif' , nodata_to_value=0)
##bins = range(1 , np.max(INT_raster) , 2)
##hist , xx = np.histogram(INT_raster , bins)
##pl.plot(xx[0:xx.size - 1] , hist)


#

#Prob_W.wet_bond(map_file_path, map_file_path , 'C_INT_O.tif')
#Prob_W.refine_wet(map_file_path, map_file_path , 'WMD.tif' , 'VB_R.tif')

#Prob_W.make_stream(map_file_path, map_file_path , 'length.tif' , 'ST.tif' , 'Fdir.tif' , 30 , 10)

#INT_PROS.wet_bond(map_file_path, map_file_path , 'C_INT_O.tif' , 'SD.tif', 80)
        
