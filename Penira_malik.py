import arcpy, mmap, os, sys, shutil, traceback
from arcpy import env, mapping, Raster
from arcpy.sa import *
import numpy as np
from arcpy import env
import warnings
import scipy
from scipy import signal
 
def anisodiff(dem_Array, num_iter  , edge_thresh , increment , diff_S_S , resolution ,option):

##        grad_x , grad_y =  np.gradient(dem_Array , resolution)
##        abs_grad = np.sqrt(grad_x ** 2 + grad_y ** 2)
##        edge_thresh = np.percentile(abs_grad , 90)
##
##        edge_thresh = 20

        print edge_thresh

        Ni =  dem_Array.shape[0]
        Nj =  dem_Array.shape[1]

        for it in range (0 , num_iter):
                print it
                
                if diff_S_S > 0:
                        org_dem_Array = np.copy(dem_Array)
                        dem_Array = scipy.ndimage.filters.gaussian_filter(dem_Array , diff_S_S)
                        #dem_Array = qussian_filter(dem_Array , 5 , diff_S_S)
                
                Ni =  dem_Array.shape[0]
                Nj =  dem_Array.shape[1]
                
                # Calculate drevtives in for directions
                delta_n = (np.append(dem_Array[0 , :].reshape(1 , Nj) , dem_Array[0 : Ni - 1  , :].reshape(Ni - 1 , Nj) , axis = 0) - dem_Array) / resolution
                delta_s = (np.append(dem_Array[1 : Ni  , :].reshape(Ni - 1 , Nj) , dem_Array[Ni - 1 , :].reshape(1 , Nj) , axis = 0) - dem_Array) / resolution
                delta_e = (np.append(dem_Array[: , 1 : Nj].reshape(Ni , Nj - 1) , dem_Array[: , Nj - 1].reshape(Ni , 1) , axis = 1) - dem_Array) / resolution
                delta_w = (np.append(dem_Array[: , 0].reshape(Ni , 1) , dem_Array[: , 0 : Nj - 1].reshape(Ni , Nj - 1) , axis = 1) - dem_Array) / resolution
                delta_n = np.nan_to_num(delta_n)
                delta_s = np.nan_to_num(delta_s)
                delta_e = np.nan_to_num(delta_e)
                delta_w = np.nan_to_num(delta_w)

                if option == 1:
                        Cn = np.exp(-(np.abs(delta_n)/edge_thresh)**2.)
                        Cs = np.exp(-(np.abs(delta_s)/edge_thresh)**2.)
                        Ce = np.exp(-(np.abs(delta_e)/edge_thresh)**2.)
                        Cw = np.exp(-(np.abs(delta_w)/edge_thresh)**2.)
                        del delta_n , delta_s , delta_e , delta_w
                elif option == 2:
                        Cn = 1./(1.+(np.abs(delta_n)/edge_thresh)**2.)
                        Cs = 1./(1.+(np.abs(delta_s)/edge_thresh)**2.)
                        Ce = 1./(1.+(np.abs(delta_e)/edge_thresh)**2.)
                        Cw = 1./(1.+(np.abs(delta_w)/edge_thresh)**2.)
                        del delta_n , delta_s , delta_e , delta_w

                if diff_S_S > 0:
                        delta_n = (np.append(org_dem_Array[0 , :].reshape(1 , Nj) , org_dem_Array[0 : Ni - 1  , :].reshape(Ni - 1 , Nj) , axis = 0) - org_dem_Array) / resolution
                        delta_s = (np.append(org_dem_Array[1 : Ni  , :].reshape(Ni - 1 , Nj) , org_dem_Array[Ni - 1 , :].reshape(1 , Nj) , axis = 0) - org_dem_Array) / resolution
                        delta_e = (np.append(org_dem_Array[: , 1 : Nj].reshape(Ni , Nj - 1) , org_dem_Array[: , Nj - 1].reshape(Ni , 1) , axis = 1) - org_dem_Array) / resolution
                        delta_w = (np.append(org_dem_Array[: , 0].reshape(Ni , 1) , org_dem_Array[: , 0 : Nj - 1].reshape(Ni , Nj - 1) , axis = 1) - org_dem_Array) / resolution
                        delta_n = np.nan_to_num(delta_n)
                        delta_s = np.nan_to_num(delta_s)
                        delta_e = np.nan_to_num(delta_e)
                        delta_w = np.nan_to_num(delta_w)
                        

                dem_Array = dem_Array +  increment * (np.multiply(Cn, delta_n) + np.multiply(Cs, delta_s) + np.multiply(Ce, delta_e) + np.multiply(Cw, delta_w))
                del delta_n , delta_s , delta_e , delta_w , Cn , Cs , Ce , Cw
        
        return dem_Array

##########################################################
##########################################################

def qussian_filter(dem_Array , kernel_W , diff_S_S):
        Ni =  dem_Array.shape[0]
        Nj =  dem_Array.shape[1]

        Half_kernel_W = (kernel_W - 1) / 2

        x = np.ones((kernel_W , 1)) * range(-Half_kernel_W , Half_kernel_W + 1)
        y = np.transpose(x)

        g_Filter = np.exp(-(x ** 2 + y ** 2) / (2 * diff_S_S))
        g_Filter = g_Filter /np.sum(g_Filter)

        #xL = np.average(dem_Array[: , 0 : Half_kernel_W] , axis = 1)
        #xR = np.average(dem_Array[: , Nj - Half_kernel_W : Nj] , axis = 1)

        #temp_dem_Array = np.concatenate((xL.reshape(Ni , 1) * np.ones((Ni , 2)), dem_Array , xR.reshape(Ni , 1) * np.ones((Ni , 2))), axis=1)

        #xU = np.average(temp_dem_Array[0 : Half_kernel_W , :] , axis = 0)
        #xD = np.average(temp_dem_Array[Ni - Half_kernel_W : Ni , :] , axis = 0)

        #temp_Ni =  temp_dem_Array.shape[0]
        #temp_Nj =  temp_dem_Array.shape[1]

        #temp_dem_Array = np.concatenate((xU.reshape(1 , temp_Nj) * np.ones((2 , temp_Nj)), temp_dem_Array , xD.reshape(1 , temp_Nj) * np.ones((2 , temp_Nj))), axis=0)
        
        #smooth_dem_Array = scipy.signal.convolve2d(temp_dem_Array , g_Filter, mode='full', boundary='fill', fillvalue=0)
        smooth_dem_Array = np.convolve(dem_Array , g_Filter)
        #smooth_dem_Array = scipy.ndimage.filters.convolve(dem_Array , g_Filter)
        #smooth_dem_Array = scipy.signal.convolve2d(dem_Array , g_Filter, mode='same', boundary='symm', fillvalue=0)

        return smooth_dem_Array
        
        
def Penira_malik(in_file_path, out_file_path , num_iter , edge_thresh , increment,   in_raster , out_raster , option):
        
        IN_RASTER = '%s'%in_file_path +  '/' + '%s'%in_raster

        raster = arcpy.RasterToNumPyArray(IN_RASTER)
        corner = arcpy.Point(arcpy.Describe(IN_RASTER).Extent.XMin,arcpy.Describe(IN_RASTER).Extent.YMin)
        dx = arcpy.Describe(IN_RASTER).meanCellWidth

        #Filt_DEM = scipy.ndimage.filters.gaussian_filter(raster , 2)

        Filt_DEM  = anisodiff(raster, num_iter  , edge_thresh , increment , 0.1 , 1 , option)
       
        Con(arcpy.NumPyArrayToRaster(Filt_DEM ,corner,dx , dx)\
        > 0.01 , arcpy.NumPyArrayToRaster(Filt_DEM ,corner,dx , dx))\
        .save('%s'%out_file_path +  '/' + '%s'%out_raster)

                    




